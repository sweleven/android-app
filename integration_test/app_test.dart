// ignore_for_file: unused_local_variable
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:block_covid/main.dart' as app;

void main() {
  group('App test', () {
    IntegrationTestWidgetsFlutterBinding.ensureInitialized();

    //HomeCleaner screen
    final exitButton = find.byKey(Key('ExitButton'));

    //LogOut Dialog
    final logoutDialog = find.byKey(Key('LogoutDialog'));
    final checkButton = find.byKey(Key('checkButton'));
    final closeButton = find.byKey(Key('closeButton'));

    //Tasto Stanza usate
    final stanzeUsateButton = find.byKey(Key('StanzeUsateButton'));

    //Tasto Stanze igienizzate
    final stanzeIgienizzateButton = find.byKey(Key('StanzeIgienizzateButton'));

    //DialogNfcReadIgienizzatore
    final nfcButton = find.byKey(Key('NfcButton'));
    final nfcButton1 = find.byType(IconButton).first;
    final nfcDialog = find.byKey(Key('NfcDialog'));
    final exitButtonNfcDialog = find.byKey(Key('ExitButtonNfcDialog'));
    final exitButtonNfcDialog1 = find.text('Exit');
    final nfcData = find.byType(NfcData);
    final nav = find.byType(Navigator);

    //DialogRisultatoIgienizzatore

    testWidgets('Logout NON andato a buon fine', (tester) async {
      app.main();
      await tester.pumpAndSettle();

      await tester.tap(exitButton);
      await tester.pumpAndSettle();

      await tester.tap(closeButton);
      await tester.pumpAndSettle();
    });

    testWidgets('Tap Stanza igienizzata', (tester) async {
      app.main();
      await tester.pumpAndSettle();

      await tester.tap(stanzeIgienizzateButton);
      await tester.pumpAndSettle();
    });

    testWidgets('Tap Stanza usata', (tester) async {
      app.main();
      await tester.pumpAndSettle();

      await tester.tap(stanzeUsateButton);
      await tester.pumpAndSettle();
    });
    testWidgets('NFC stanze usate: igienizzazione non fatta', (tester) async {
      app.main();
      await tester.pumpAndSettle();

      //await tester.tap(stanzeUsateButton);
      //await tester.pumpAndSettle();

      await tester.tap(nfcButton1);

      await tester.pumpAndSettle();
      expect(nfcDialog, findsOneWidget);
      await tester.tap(exitButtonNfcDialog);
      await tester.pumpAndSettle();

      // await tester.enterText(nfcData, 'stanza1');
      // await tester.tap(exitButtonNfcDialog);
      // await tester.pumpAndSettle();
      // await tester.tap(nav);
      // await tester.pumpAndSettle();
    });
  });
}
