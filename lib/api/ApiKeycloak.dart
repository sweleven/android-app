import 'dart:convert';
import 'package:block_covid/api/ApiStorage.dart';
import 'package:http/http.dart' as http;
import 'package:openid_client/openid_client_io.dart';
import 'package:url_launcher/url_launcher.dart';

/// Classe che gestisce l'interazione con Keycloak
class ApiKeycloak {
  ApiKeycloak()
      : _baseUrlHttp = '192.168.210.40',
        _baseIdentities = '/api/identities' {
    _uriRealm = Uri.parse('http://192.168.210.40:8080/auth/realms/sweleven');
    _uriRefresh =
        Uri.parse(_uriRealm.toString() + '/protocol/openid-connect/token');
    _clientId = 'flutter-client';
  }
  Uri _uriRealm;
  Uri _uriRefresh;
  final String _baseUrlHttp;
  final String _baseIdentities;
  String _clientId;

  static Map _datiUtente;

  ApiStorage _apiStorage = ApiStorage();

  /// Non usare. Chiamare invece [logIn()].
  ///
  /// Permette di effettuare il login aprendo una [WebView] a sè stante.
  /// Ritorna le [Credential] dell'utente loggato.
  Future<Credential> _logInWeb() async {
    Issuer issuer = await Issuer.discover(_uriRealm);
    Client client = Client(issuer, _clientId);

    urlLauncher(String url) async {
      if (await canLaunch(url)) {
        await launch(url,
            forceWebView: true, enableJavaScript: true, enableDomStorage: true);
      } else {
        throw 'Impossibile lanciare $url';
      }
    }

    Authenticator authenticator =
        Authenticator(client, urlLancher: urlLauncher);
    Credential result = await authenticator.authorize();
    closeWebView();
    return result;
  }

  /// Effettua il login dell'utente.
  ///
  /// Cerca in locale il token salvato, se non lo trova o non
  /// è valido mostra la pagina di login.
  /// Ritorna la mappa contenente tutti i dati del token valido
  /// con aggounto il logoutUrl per fare il logout.
  Future<Map<String, dynamic>> logIn() async {
    bool accessTokenSalvato = await _apiStorage.getAccessToken() != null;
    bool refreshFallito = false;
    Credential credential;
    if (accessTokenSalvato) {
      print('Login usando il token salvato');

      final tt = await _apiStorage.getTokenType();
      final rt = await _apiStorage.getRefreshToken();
      final it = await _apiStorage.getIdToken();
      Issuer issuer;
      try {
        issuer = await Issuer.discover(_uriRealm);
      } catch (e) {
        print(e.toString());
        return null;
      }

      Client client = Client(issuer, _clientId);
      credential = client.createCredential(
        accessToken:
            null, // metto null così lo obbligo a fare il refresh del token
        tokenType: tt,
        refreshToken: rt,
        idToken: it,
      );

      // Prova a validare il token salvato
      credential.validateToken();
      try {
        _salvaToken((await credential.getTokenResponse()));
      } catch (e) {
        print('Errore durante il refresh del token vecchio');
        refreshFallito = true;
      }
    }
    if (!accessTokenSalvato || refreshFallito) {
      print('Login usando un nuovo token');
      credential = await _logInWeb();
      _salvaToken(await credential.getTokenResponse());
    }
    TokenResponse token = await credential.getTokenResponse();
    Map<String, dynamic> mappa = _convertiToken(token.accessToken);
    // Aggiungo alla mappa il logoutUrl, così poi nell'app lo posso usare.
    mappa.addAll({'logoutUrl': credential.generateLogoutUrl().toString()});
    _datiUtente = mappa;
    return _datiUtente;
  }

  /// Dato l'access token in entrata, ritorna una Map con tutte le proprietà
  /// salvate nel token.
  Map<String, dynamic> _convertiToken(String token) {
    if (token == null) throw 'Il token è null';
    final List<String> parts = token.split('.');
    if (parts.length != 3) throw 'Il token è malformato';
    final String normalizzato = base64Url.normalize(parts[1]);
    final String risposta = utf8.decode(base64Url.decode(normalizzato));
    final rispostaMappata = json.decode(risposta);
    if (rispostaMappata is! Map<String, dynamic>)
      throw 'Risposta non mappata correttamente';
    return rispostaMappata;
  }

  /// Salva in memoria il [TokenResponse]
  Future<void> _salvaToken(TokenResponse token) async {
    _apiStorage.salvaToken(
        accessToken: token.accessToken,
        refreshToken: token.refreshToken,
        tokenType: token.tokenType,
        idToken: token.idToken.toCompactSerialization());
  }

  /// Effettua il logout dell'utente tramite il suo [LogoutUrl].
  Future<void> logOut(Uri logoutUri) async {
    http.Response risposta = await http.get(logoutUri);
    if (risposta.statusCode != 200) {
      throw 'Impossibile effettuare il logout: ' + risposta.reasonPhrase;
    }
    _apiStorage.azzeraToken();
    print('Logout effettuato');
  }

  /// Ritorna l'access token aggiornato dell'utente loggato attualmente.
  ///
  /// Se l'access token scade dopo 30+ secondi non lo aggiorna.
  Future<String> getAccessToken() async {
    if (_datiUtente == null) {
      return '';
    }
    String accessToken = await _apiStorage.getAccessToken();
    Map<String, dynamic> m = _convertiToken(accessToken);
    DateTime scadenza = DateTime.fromMillisecondsSinceEpoch(
        int.parse((m['exp']).toString() + '000'));
    if ((DateTime.now().add(Duration(seconds: 30)).isBefore(scadenza))) {
      return accessToken;
    }
    await _updateToken();
    return await _apiStorage.getAccessToken();
  }

  String getLogoutUrl() {
    return _datiUtente['logoutUrl'];
  }

  /// Aggiorna i tokens dell'utente attualmente loggato, e li salva in memoria.
  Future<void> _updateToken() async {
    print('Richiedo un update del AccessToken');
    final http.Response risposta = await http.post(_uriRefresh, body: {
      'client_id': _clientId,
      'grant_type': 'refresh_token',
      'refresh_token': await _apiStorage.getRefreshToken()
    });
    if (risposta.statusCode != 200) {
      // Se entro qua vuol dire che il refresh token è scaduto e devo rifare il login.
      await logIn();
      return;
    }
    Map<String, dynamic> tokensNuovi = jsonDecode(risposta.body);
    Map<String, dynamic> mappaNuova =
        _convertiToken(tokensNuovi['access_token']);
    await _apiStorage.salvaToken(
        accessToken: tokensNuovi['access_token'],
        refreshToken: tokensNuovi['refresh_token'],
        tokenType: tokensNuovi['token_type'],
        idToken: tokensNuovi['id_token']);
    mappaNuova.addAll({'logoutUrl': _datiUtente['logoutUrl']});
    _datiUtente = mappaNuova;
  }

  Future<bool> modificaPassword() async {
    final http.Response risposta = await http.get(
        Uri.http(_baseUrlHttp, _baseIdentities + '/profile/resetPassword'),
        headers: {'Authorization': 'Bearer ' + await getAccessToken()});
    if (risposta.statusCode == 200) {
      return true;
    }
    return false;
  }
}
