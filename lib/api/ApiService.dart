import 'package:block_covid/api/ApiKeycloak.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:block_covid/model/Stanza.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

/// Classe per gestire le Api usate dai cleaner e in parte dai dipendenti.
class ApiService {
  ApiService()
      : _baseUrlHttp = '192.168.210.40',
        _baseInventory = '/api/inventory',
        _baseCleanings = '/api/cleanings' {
    //_baseUrlInventory = 'http://' + _baseUrlHttp + _baseInventory;
  }
  final String _baseUrlHttp;
  final String _baseInventory;
  final String _baseCleanings;
  //String _baseUrlInventory;
  final ApiKeycloak _apiKeycloak = ApiKeycloak();

  /// Pulisce la postazione o stanza identificata da [tag].
  ///
  /// Ritorna [true] se la pulizia va a buon fine.
  /// Ritorna [false] altrimenti.
  Future<bool> pulisci(http.Client client, String tag) async {
    final http.Response rispostaPost = await client.post(
        Uri.http(
          _baseUrlHttp,
          _baseCleanings + '/cleanings',
        ),
        body: jsonEncode({'rfid': tag}),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
        });
    if (rispostaPost.statusCode == 200 || rispostaPost.statusCode == 201) {
      return true;
    }
    return false;
  }

  /// Ritorna la stanza, dato il suo [tag] rfid.
  Future<Stanza> getStanzaFromRfid(http.Client client, String tag) async {
    final http.Response risposta = await client.get(
        Uri.http(
          _baseUrlHttp,
          _baseInventory + '/rooms/rfid/$tag',
        ),
        headers: {
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
        });
    if (risposta.statusCode != 200) {
      return null;
    }
    Map stanza = json.decode(risposta.body);

    List<Postazione> postazioni = List<Postazione>.empty(growable: true);
    stanza['workspaces'].forEach((p) {
      postazioni.add(Postazione(
          id: p['_id'],
          name: p['name'],
          room: p['room'],
          rfid: p['rfid'],
          clean: p['clean']));
    });
    return Stanza(
        id: stanza['_id'],
        listapostazioni: postazioni,
        name: stanza['mane'],
        rfid: stanza['rfid'],
        totalepostazioni: stanza['workspaces_total'],
        v: stanza['__v']);
  }

  /// Ritorna tutte le stanze con nome contenente [nome].
  Future<List<Stanza>> getRooms(http.Client client, {String nome = ''}) async {
    final http.Response risposta = await client.get(
        Uri.http(_baseUrlHttp, _baseInventory + '/rooms', {'name': nome}),
        headers: {
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
        });
    List rispostaLista = json.decode(risposta.body);
    List<Stanza> ris = List<Stanza>.empty(growable: true);
    rispostaLista.forEach((s) {
      List<Postazione> postazioni = List<Postazione>.empty(growable: true);
      s['workspaces'].forEach((p) {
        postazioni.add(Postazione(
            id: p['_id'],
            name: p['name'],
            room: p['room'],
            rfid: p['rfid'],
            clean: p['clean']));
      });
      ris.add(Stanza(
          id: s['_id'],
          name: s['name'],
          v: s['__v'],
          rfid: s['rfid'],
          listapostazioni: postazioni,
          totalepostazioni: s['workspaces_total']));
    });
    return ris;
  }

  /// Ritorna la postazione dato il suo id
  Future<Postazione> getPostazioneFromId(http.Client client, String id) async {
    final http.Response risposta = await client.get(
        Uri.http(_baseUrlHttp, _baseInventory + '/workspaces/$id'),
        headers: {
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
        });
    if (risposta.statusCode != 200) {
      return null;
    }
    Map risp = json.decode(risposta.body);
    return Postazione(
        id: risp['_id'],
        name: risp['name'],
        rfid: risp['rfid'],
        room: risp['room'],
        clean: risp['clean']);
  }
}
