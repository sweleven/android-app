import 'package:block_covid/api/ApiKeycloak.dart';
import 'package:block_covid/api/ApiService.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:block_covid/model/Stanza.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ApiServiceCleaner {
  ApiServiceCleaner()
      : _baseUrlHttp = '192.168.210.40',
        _baseCleanings = '/api/cleanings',
        _baseInventory = '/api/inventory';
  final String _baseUrlHttp;
  final String _baseInventory;
  final String _baseCleanings;

  final ApiKeycloak _apiKeycloak = ApiKeycloak();
  final ApiService _apiService = ApiService();

  Future<List<Stanza>> getStanzeSporche(http.Client client) async {
    final http.Response risposta = await client.get(
        Uri.http(
          _baseUrlHttp,
          _baseInventory + '/workspaces/dirty',
        ),
        headers: {
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
        });
    if (risposta.statusCode != 200) {
      return null;
    }
    if (risposta.body == '') {
      return List<Stanza>.empty();
    }
    List<dynamic> rispostaLista = json.decode(risposta.body);
    List<String> listaIdStanzeSporche = List<String>.empty(growable: true);
    rispostaLista.forEach((element) {
      if (!listaIdStanzeSporche.contains(element['room'])) {
        listaIdStanzeSporche.add(element['room']);
      }
    });
    List<Stanza> listaStanzeSporche = List<Stanza>.empty(growable: true);
    List<Stanza> listaTutteStanze = await _apiService.getRooms(client);
    listaTutteStanze.forEach((e) {
      if (listaIdStanzeSporche.contains(e.id)) {
        listaStanzeSporche.add(e);
      }
    });
    return listaStanzeSporche;
  }

  /// Ottiene una mappa con due liste: una [List<Stanza>] con le stanze igienizzate oggi,
  /// e una [List<DateTime>] con l'orario di igienizzazione della relativa stanza nella stessa
  /// posizione.
  Future<Map<String, List>> getStanzePuliteOggi(http.Client client) async {
    final http.Response risposta = await client.get(
        Uri.http(
          _baseUrlHttp,
          _baseCleanings + '/cleanings',
        ),
        headers: {
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
        });
    List<dynamic> rispostaLista = json.decode(risposta.body);
    List<String> listaIdWorkspaceOggi = List<String>.empty(growable: true);
    List<DateTime> tmpOrario = List<DateTime>.empty(growable: true);
    List<DateTime> risOrario = List<DateTime>.empty(growable: true);
    List<Stanza> risStanze = List<Stanza>.empty(growable: true);
    DateTime oggi = DateTime.now();
    rispostaLista.forEach((element) {
      DateTime pulizia =
          DateTime.fromMillisecondsSinceEpoch(element['datetime'], isUtc: true);
      if (pulizia.year == oggi.year &&
          pulizia.month == oggi.month &&
          pulizia.day == oggi.day) {
        tmpOrario.add(pulizia);
        listaIdWorkspaceOggi.add(element['workspace']);
      }
    });
    List<String> listaIdRoomSporche = List<String>.empty(growable: true);
    Postazione p;
    for (int a = 0; a < listaIdWorkspaceOggi.length; a++) {
      p = await _apiService.getPostazioneFromId(
          client, listaIdWorkspaceOggi[a]);
      if (!listaIdRoomSporche.contains(p.room)) {
        risOrario.add(tmpOrario[a]);
        listaIdRoomSporche.add(p.room);
      }
    }
    List<Stanza> listaTotStanze = await _apiService.getRooms(client);
    listaIdRoomSporche.forEach((element) {
      for (int a = 0; a < listaTotStanze.length; a++) {
        if (listaTotStanze[a].id == element) {
          risStanze.add(listaTotStanze[a]);
        }
      }
    });
    Map<String, List> ris = Map<String, List>();
    ris.addAll({'listaStanze': risStanze, 'listaOrari': risOrario});
    return ris;
  }
}
