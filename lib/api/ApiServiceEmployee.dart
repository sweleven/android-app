import 'package:block_covid/api/ApiKeycloak.dart';
import 'package:block_covid/api/ApiService.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:block_covid/model/Prenotazione.dart';
import 'package:block_covid/model/StatoPostazioneScan.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

/// Classe per gestire le Api usate solo dai dipendenti.
class ApiServiceEmployee {
  ApiServiceEmployee()
      : _baseUrlHttp = '192.168.210.40',
        _baseInventory = '/api/inventory',
        _baseBooking = '/api/booking' {
    _baseUrlBooking = 'http://' + _baseUrlHttp + _baseBooking;
  }
  final String _baseUrlHttp;
  final String _baseBooking;
  final String _baseInventory;
  String _baseUrlBooking;

  final ApiKeycloak _apiKeycloak = ApiKeycloak();
  final ApiService _apiService = ApiService();

  /// Dichiara l'inizio di utilizzo di una [postazione].
  /// Se la postazione è sporca la pulisce.
  ///
  /// Ritorna [true] se è andato a buon fine, [false] altrimenti.
  Future<bool> inizioUso(http.Client client, Postazione postazione) async {
    bool pulizia = true;
    if (postazione.stato == StatoPostazioneScan.miaInizioSporca) {
      pulizia = await _apiService.pulisci(client, postazione.rfid);
    }
    final http.Response rispostaPost = await client.post(
        Uri.http(
          _baseUrlHttp,
          _baseBooking + '/check-in',
        ),
        body: jsonEncode(<String, String>{'rfid': postazione.rfid}),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
        });

    return true &&
        pulizia &&
        (rispostaPost.statusCode == 200 || rispostaPost.statusCode == 201);
  }

  /// Dichiara la fine dell'uso di una postazione, specificando ce la postazione viene anche
  /// pulita o no.
  ///
  /// Ritorna [true] se è andato a buon fine, [false] altrimenti.
  Future<bool> fineUso(
      http.Client client, Postazione postazione, bool pulizia) async {
    bool pulita = true;
    if (pulizia) {
      pulita = await _apiService.pulisci(client, postazione.rfid);
    }
    final http.Response rispostaPost = await client.post(
        Uri.http(
          _baseUrlHttp,
          _baseBooking + '/check-out',
        ),
        body: jsonEncode(<String, String>{'rfid': postazione.rfid}),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
        });
    return (rispostaPost.statusCode == 200 || rispostaPost.statusCode == 201) &&
        pulita;
  }

  /// Ritorna la mappa con tutte le prenotazioni della persona, tra
  /// [inizio] e [fine].
  ///
  /// [cancellate] indica se voglio avere anche le prenotazioni annullate
  Future<List<Prenotazione>> getPrenotazioni(http.Client client,
      DateTime inizio, DateTime fine, bool cancellate) async {
    inizio = inizio.subtract(Duration(hours: 2));
    fine = fine.subtract(Duration(hours: 2));
    final risposta = await client.get(
        Uri.http(_baseUrlHttp, _baseBooking + '/bookings', {
          'start_datetime': inizio.toIso8601String().substring(0, 23) + 'Z',
          'end_datetime': fine.toIso8601String().substring(0, 23) + 'Z',
          'show_cancelled': cancellate.toString()
        }),
        headers: {
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
        });
    if (risposta.statusCode != 200) {
      return List<Prenotazione>.empty();
    }
    List rispDecodificata = json.decode(risposta.body);
    List<Prenotazione> ris = List<Prenotazione>.empty(growable: true);
    for (int a = 0; a < rispDecodificata.length; a++) {
      if (rispDecodificata[a]['consumed'] && !cancellate) {
        continue;
      }
      Postazione postazione = await _apiService.getPostazioneFromId(
          client, rispDecodificata[a]['workspace']);
      Prenotazione prenotazione = Prenotazione(
          id: rispDecodificata[a]['_id'],
          postazione: postazione,
          consumed: rispDecodificata[a]['consumed'],
          inizio: DateTime.fromMillisecondsSinceEpoch(
              rispDecodificata[a]['start_datetime'],
              isUtc: true),
          fine: DateTime.fromMillisecondsSinceEpoch(
              rispDecodificata[a]['end_datetime'],
              isUtc: true),
          cancellazione: rispDecodificata[a]['cancellation_datetime'] != null
              ? DateTime.fromMillisecondsSinceEpoch(
                  rispDecodificata[a]['cancellation_datetime'],
                  isUtc: true)
              : null);
      ris.add(prenotazione);
    }
    return ris;
  }

  /// Efettua una prenotazione se possibile.
  ///
  /// Ritorna [true] se ha prenotato, [false] altrimenti.
  Future<bool> createReservation(http.Client client, Prenotazione p) async {
    //mettere gli altri dati nome timestamp... in json serializzarli e mandare

    final response = await client.post(Uri.parse(_baseUrlBooking + '/bookings'),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken(),
        },
        body: jsonEncode(<String, String>{
          'workspace': p.postazione.id,
          'start_datetime': (p.inizio.subtract(Duration(hours: 2)))
                  .toIso8601String()
                  .substring(0, 23) +
              'Z',
          'end_datetime': (p.fine.subtract(Duration(hours: 2)))
                  .toIso8601String()
                  .substring(0, 23) +
              'Z'
        }));
    if (response.statusCode == 200 || response.statusCode == 201) {
      print('POST prenotazione ok, codice ' + response.statusCode.toString());
      if (response.body.length > 1) {
        return true;
      } else {
        print('POST prenotazione senza body');
        return false;
      }
    }
    print('POST prenotazione errore, codice ' + response.statusCode.toString());
    return false;
  }

  Future<bool> eliminaPrenotazione(http.Client client, Prenotazione p) async {
    final http.Response risposta = await client
        .delete(Uri.parse(_baseUrlBooking + '/bookings/' + p.id), headers: {
      'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
    });
    if (risposta.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  /// Ritorna la lista di postazioni disponibili in una [Stanza] data e disponibili
  /// tra [DateTime] inizio e [DateTime] fine.
  Future<List<Postazione>> postazioniDisponibili(http.Client client,
      String idStanza, DateTime inizio, DateTime fine) async {
    final http.Response risposta = await client.get(
        Uri.http(_baseUrlHttp, _baseBooking + '/availability/workspaces', {
          'start_datetime': inizio.toIso8601String().substring(0, 23) + 'Z',
          'end_datetime': fine.toIso8601String().substring(0, 23) + 'Z'
        }),
        headers: {
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken(),
        });
    List rispostaLista = json.decode(risposta.body);
    List<Postazione> ris = List<Postazione>.empty(growable: true);
    rispostaLista.forEach((p) {
      if (p['room'] == idStanza) {
        ris.add(Postazione.fromJson(p));
      }
    });
    return ris;
  }

  /// Ritorna la postazione dato il tag NFC.
  /// Ritorna [null] se il tag è sbagliato.
  Future<Postazione> getPostazioneFromRfid(
      http.Client client, String tag) async {
    final http.Response risposta = await client.get(
        Uri.http(
          _baseUrlHttp,
          _baseInventory + '/workspaces/rfid/$tag',
        ),
        headers: {
          'Authorization': 'Bearer ' + await _apiKeycloak.getAccessToken()
        });
    if (risposta.statusCode != 200) {
      return null;
    }
    Map<String, dynamic> postazione = json.decode(risposta.body);
    return Postazione(
        id: postazione['_id'],
        name: postazione['name'],
        room: postazione['room'],
        rfid: tag,
        clean: postazione['clean'],
        stato: await _statoPostazione(postazione));
  }

  Future<StatoPostazioneScan> _statoPostazione(Map<String, dynamic> p) async {
    if (p['available'] == true) {
      // non è di nessuno
      if (p['clean'] == true) {
        // di nessuno e pulita
        return StatoPostazioneScan.diNessunoPulita;
      } else {
        // di nessuno e sporca
        return StatoPostazioneScan.diNessunoSporca;
      }
    } else {
      // è prenotata da qualche parte
      List<Prenotazione> listaPrenotazioni = await getPrenotazioni(
          http.Client(),
          DateTime.now(),
          DateTime.now().add(Duration(minutes: 1)),
          true);
      bool prenotataDaMe = false;
      Prenotazione pRiferita;
      listaPrenotazioni.forEach((element) {
        if (element.postazione.rfid == p['rfid']) {
          prenotataDaMe = true;
          pRiferita = element;
        }
      });
      if (prenotataDaMe) {
        if (pRiferita.consumed) {
          // mia, gia fatto checkin
          return StatoPostazioneScan.miaFine;
        } else {
          // prima volta che scannerizzo
          if (p['clean']) {
            return StatoPostazioneScan.miaInizioPulita;
          } else {
            return StatoPostazioneScan.miaInizioSporca;
          }
        }
      } else {
        return StatoPostazioneScan.giaPrenotata;
      }
    }
  }
}
