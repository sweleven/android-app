import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ApiStorage {
  final _storage = FlutterSecureStorage();

  Future<String> getAccessToken() async {
    return await _storage.read(key: 'accessToken');
  }

  Future<String> getTokenType() async {
    return await _storage.read(key: 'tokenType');
  }

  Future<String> getRefreshToken() async {
    return await _storage.read(key: 'refreshToken');
  }

  /// Legge il token salvato in memoria.
  Future<String> getIdToken() async {
    return await _storage.read(key: 'idToken');
  }

  /// Permette di salvare il token in memoria.
  ///
  /// [idToken] va serializzato con [toCompactSerialization()] prima
  /// di passarlo alla funzione.
  Future<void> salvaToken({
    String accessToken,
    String refreshToken,
    String tokenType,
    String idToken,
  }) async {
    await _storage.write(key: 'accessToken', value: accessToken);
    await _storage.write(key: 'refreshToken', value: refreshToken);
    await _storage.write(key: 'tokenType', value: tokenType);
    await _storage.write(key: 'idToken', value: idToken);
  }

  /// Azzera tutte le informazioni salvate.
  Future<void> azzeraToken() async {
    await _storage.deleteAll();
  }
}
