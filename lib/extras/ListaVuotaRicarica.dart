import 'package:flutter/material.dart';

/// Classe shortcut per ritornare un messaggio con sotto il pulsante di ricarica.
///
/// Testo è il testo da mostrare.
///
/// pressed DEVE essere:
/// () {setState(() {});}
class ListaVuotaRicarica extends StatefulWidget {
  ListaVuotaRicarica({@required this.testo, @required this.pressed});
  final String testo;
  final Function pressed;
  @override
  _ListaVuotaRicaricaState createState() => _ListaVuotaRicaricaState();
}

class _ListaVuotaRicaricaState extends State<ListaVuotaRicarica> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(15),
          child: Text(widget.testo,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.red, fontSize: 30)),
        ),
        ElevatedButton.icon(
            onPressed: widget.pressed,
            icon: Icon(Icons.refresh),
            label: Text('Ricarica'))
      ],
    );
  }
}
