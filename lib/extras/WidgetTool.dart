import 'package:block_covid/extras/dialogs/DialogSiNo.dart';
import 'package:block_covid/api/ApiKeycloak.dart';
import 'package:block_covid/pages/Login.dart';

import 'package:flutter/material.dart';

class WidgetTool extends StatelessWidget {
  final ApiKeycloak _apiKeycloak = ApiKeycloak();
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: IconButton(
              icon: Icon(Icons.logout),
              onPressed: () => _showLogOutDialog(context)),
        ),
        Expanded(
          child: IconButton(
              onPressed: () => _showEditPswDialog(context),
              icon: Icon(Icons.edit)),
        ),
      ],
    );
  }

  /// Mostra un dialog chiedendo se
  /// si vuole veramente effettuare il logout.
  void _showLogOutDialog(BuildContext context) async {
    bool risultato = await showDialog(
        context: context,
        builder: (context) => DialogSiNo(
              testoTitolo: 'Logout',
              testoCorpo:
                  'Vuoi davvero effettuare il logout?\nVerrai reindirizzato alla pagina di login.',
            ));
    if (risultato != null && risultato == true) {
      await _apiKeycloak.logOut(Uri.parse(_apiKeycloak.getLogoutUrl()));
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => Login()));
    }
  }

  void _showEditPswDialog(BuildContext context) async {
    bool risultato = await showDialog(
        context: context,
        builder: (context) => DialogSiNo(
              testoTitolo: 'Modifica password',
              testoCorpo: 'Vuoi richiedere il link di modifica password?',
            ));
    if (risultato != null && risultato == true) {
      await _apiKeycloak.modificaPassword();
    }
  }
}
