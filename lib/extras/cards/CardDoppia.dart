import 'package:flutter/material.dart';

class CardDoppia extends StatelessWidget {
  CardDoppia({@required this.widget1, @required this.widget2});
  final Widget widget1;
  final Widget widget2;
  @override
  Card build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(5),
      color: Colors.grey[100],
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          children: [Expanded(child: widget1), Expanded(child: widget2)],
        ),
      ),
    );
  }
}
