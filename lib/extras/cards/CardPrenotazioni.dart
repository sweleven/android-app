import 'package:block_covid/model/Prenotazione.dart';
import 'package:block_covid/pages/DettaglioPrenotazione.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CardPrenotazioni extends StatelessWidget {
  CardPrenotazioni({@required this.prenotazione, @required this.eliminabile});

  final Prenotazione prenotazione;
  final bool eliminabile;

  @override
  Card build(BuildContext context) {
    return Card(
        child: ListTile(
      leading: Icon(prenotazione.cancellazione == null
          ? Icons.person
          : Icons.person_remove),
      title: Text((prenotazione.isComputer ? 'Computer ' : 'Stanza ') +
          prenotazione.postazione.name),
      subtitle: Text(DateFormat('yyyy-MM-dd HH:mm:ss')
          .format(prenotazione.inizio.add(Duration(hours: 2)))),
      onTap: () => _cardTappata(context),
      trailing: Icon(Icons.arrow_forward_ios),
    ));
  }

  //trovare il modo di far refreshare il parent di questa card (HomeListaPrenotazioni) quando il navigator mi ritorna true (prenotazione eliminata con successo)
  void _cardTappata(BuildContext context) async {
    // ignore: unused_local_variable
    bool resultDelete = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DettaglioPrenotazione(
                  prenotazione: prenotazione,
                  eliminabile: eliminabile,
                )));
  }
}
