import 'dart:ui';
import 'package:block_covid/api/ApiServiceEmployee.dart';
import 'package:block_covid/extras/dialogs/DialogErrore.dart';
import 'package:block_covid/extras/dialogs/DialogRispostaValida.dart';
import 'package:http/http.dart' as http;
import 'package:block_covid/model/Prenotazione.dart';
import 'package:flutter/material.dart';

///Widget di dialogo per confermare la prenotazione che gli viene passata
class DialogConfermaPrenotazione extends StatefulWidget {
  DialogConfermaPrenotazione({@required this.prenotazione});
  final Prenotazione prenotazione;
  final ApiServiceEmployee _apiServiceEmployee = ApiServiceEmployee();
  @override
  DialogConfermaPrenotazioneState createState() =>
      DialogConfermaPrenotazioneState();
}

class DialogConfermaPrenotazioneState
    extends State<DialogConfermaPrenotazione> {
  static const double fontpix = 18;
  final TextStyle stilebold =
      TextStyle(fontSize: fontpix, fontWeight: FontWeight.bold);
  final TextStyle stilenormale = TextStyle(fontSize: fontpix);
  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      key: Key("DialogConfermaPrenotazione"),
      title: Text('Riepilogo prenotazione',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      content: Container(
          height: 250,
          alignment: Alignment.centerLeft,
          child: Column(children: <Widget>[
            Text('Sei sicuro di volere prenotare per\n', style: stilenormale),
            Text.rich(TextSpan(
              text: 'il giorno: ',
              style: stilenormale,
              children: <TextSpan>[
                TextSpan(
                    text:
                        '${widget.prenotazione.inizio.toString().substring(0, 10)}\n',
                    style: stilebold)
              ],
            )),
            Text.rich(TextSpan(
              text: 'dalle: ',
              style: stilenormale,
              children: <TextSpan>[
                TextSpan(
                    text:
                        '${TimeOfDay.fromDateTime(widget.prenotazione.inizio).format(context)}\n',
                    style: stilebold)
              ],
            )),
            Text.rich(TextSpan(
              text: 'alle: ',
              style: stilenormale,
              children: <TextSpan>[
                TextSpan(
                    text:
                        '${TimeOfDay.fromDateTime(widget.prenotazione.fine).format(context)}\n',
                    style: stilebold)
              ],
            )),
            Text.rich(TextSpan(
              text: 'la postazione: ',
              style: stilenormale,
              children: <TextSpan>[
                TextSpan(
                    text: '${widget.prenotazione.postazione.name}',
                    style: stilebold),
                TextSpan(text: ' ?', style: stilenormale)
              ],
            )),
          ])),
      actions: [
        IconButton(
            key: Key("btnprenota"),
            icon: Icon(Icons.check),
            color: Colors.green,
            // Se premo ok elimino la prenotazione e faccio il pop, ritornando true se
            // la prenotazione è stata eliminata con successo, false altrimenti.
            onPressed: _checkPressed),
        IconButton(
            key: Key("cancel"),
            icon: Icon(Icons.close),
            color: Colors.red,
            onPressed: () => Navigator.of(context).pop()),
      ],
    );
  }

  /// Fa la prenotazione.
  ///
  /// Ritorna [true] se ha prenotato, [false] altrimenti
  Future<bool> _confermaPrenotazione() async {
    return await widget._apiServiceEmployee.createReservation(
        http.Client(),
        Prenotazione(
            postazione: widget.prenotazione.postazione,
            inizio: widget.prenotazione.inizio,
            fine: widget.prenotazione.fine));
  }

  void _checkPressed() async {
    bool risPrenotazione = false;
    try {
      risPrenotazione = await _confermaPrenotazione();
    } catch (e) {
      await showDialog(
          context: context,
          builder: (context) => (DialogErrore(
              risultato:
                  'Errore di connessione al server, controlla la conessione e riprova, altrimenti riprova più tardi')));
    }
    if (risPrenotazione) {
      await showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) => (DialogRispostaValida(
              risultato: 'Prenotazione effettuata con successo')));
    } else {
      await showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) =>
              (DialogErrore(risultato: 'Errore, prenotazione non effettuata')));
    }
    Navigator.of(context).pop();
  }
}
