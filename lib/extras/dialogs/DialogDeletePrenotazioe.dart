import 'package:block_covid/api/ApiServiceEmployee.dart';
import 'package:block_covid/model/Prenotazione.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class DialogDeletePrenotazione extends StatefulWidget {
  DialogDeletePrenotazione({@required this.prenotazione});
  final Prenotazione prenotazione;
  final ApiServiceEmployee _apiServiceEmployee = ApiServiceEmployee();
  @override
  _DialogDeletePrenotazioneState createState() =>
      _DialogDeletePrenotazioneState();
}

class _DialogDeletePrenotazioneState extends State<DialogDeletePrenotazione> {
  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      title: Text('Elimina prenotazione'),
      content: Text(
          'Sei sicuro di volere eliminare la prenotazione della postazione ' +
              widget.prenotazione.postazione.name +
              ' ?'),
      actions: [
        IconButton(
            icon: Icon(Icons.check),
            color: Colors.green,
            // Se premo ok elimino la prenotazione e faccio il pop, ritornando true se
            // la prenotazione è stata eliminata con successo, false altrimenti.
            onPressed: () async {
              Navigator.of(context).pop(await _eliminaPrenotazione());
            }),
        IconButton(
            icon: Icon(Icons.close),
            color: Colors.red,
            // Ritornando null invece segnalo che l'utente non ha tappato ok
            onPressed: () => Navigator.of(context).pop()),
      ],
    );
  }

  Future<bool> _eliminaPrenotazione() async {
    bool ris = await widget._apiServiceEmployee
        .eliminaPrenotazione(http.Client(), widget.prenotazione);
    if (ris) {
      return true;
    } else {
      return false;
    }
  }
}
