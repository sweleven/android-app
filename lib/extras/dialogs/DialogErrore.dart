import 'package:flutter/material.dart';

/// Dialog generico con testo e pulsante esci.
class DialogErrore extends StatelessWidget {
  DialogErrore(
      {@required this.risultato, this.icona = const Icon(Icons.error)});
  final TextStyle stiletesto =
      TextStyle(fontSize: 18, color: Colors.red, fontWeight: FontWeight.w300);

  /// Il testo da mostrare nel dialog.
  final String risultato;

  /// Icona da mostrare nel Dialog
  final Icon icona;
  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      title: Icon(Icons.error, color: Colors.red, size: 50.0),
      content: Text(risultato, style: stiletesto),
      actions: [
        IconButton(
            icon: Icon(Icons.exit_to_app),
            color: Theme.of(context).primaryColor,
            onPressed: () => Navigator.of(context).pop())
      ],
    );
  }
}
