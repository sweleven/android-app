import 'package:block_covid/api/ApiService.dart';
import 'package:block_covid/api/ApiServiceEmployee.dart';
import 'package:block_covid/extras/dialogs/DialogNfcPostReadPulizia.dart';
import 'package:block_covid/extras/dialogs/DialogRisultato.dart';
import 'package:block_covid/model/GpsManager.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:block_covid/model/StatoPostazioneScan.dart';
import 'package:block_covid/pages/PreviewPrenotazione.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

/// Dialog mostrato dopo la lettura di un tag NFC.
///
/// In base al tag letto cambia in modo dinamico.
class DialogNfcPostRead extends StatelessWidget {
  DialogNfcPostRead({@required this.postazione, @required this.gpsManager});

  /// Postazione scannerizzata.
  final Postazione postazione;
  final GpsManager gpsManager;

  final ApiServiceEmployee _apiServiceEmployee = ApiServiceEmployee();
  final ApiService _apiService = ApiService();

  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      title: _creaTitolo(),
      content: _creaContent(),
      actions: _creaActions(context),
    );
  }

  Widget _creaTitolo() {
    return Text('Postazione: ' + postazione.name);
  }

  Widget _creaContent() {
    String testo;
    switch (postazione.stato) {
      case StatoPostazioneScan.diNessunoPulita:
        testo =
            'La postazione scannerizzata è utilizzabile.\nDesideri prenotarla?';
        break;
      case StatoPostazioneScan.diNessunoSporca:
        testo =
            'La postazione scannerizzata è utilizzabile ma sporca.\nDesideri pulirla e prenotarla?';
        break;
      case StatoPostazioneScan.giaPrenotata:
        testo =
            'La postazione scannerizzata è già stata prenotata e non è quindi utilizzabile.';
        break;
      case StatoPostazioneScan.miaInizioPulita:
        testo = 'Si desidera iniziare ad usare la postazione?';
        break;
      case StatoPostazioneScan.miaInizioSporca:
        testo =
            "La postazione è sporca.\nPrima di utilizzarla è necessario pulirla.\nPulire la postazione e inziarne l'utilizzo?";
        break;
      case StatoPostazioneScan.miaFine:
        testo = "Si desidera interrompere l'utilizzo della postazione?";
        break;
      default:
        testo = 'Errore :(\nRiprova grazie.';
        break;
    }
    return Text(testo);
  }

  List<Widget> _creaActions(BuildContext context) {
    List<Widget> ris;
    switch (postazione.stato) {
      case StatoPostazioneScan.diNessunoPulita:
        ris = [
          TextButton.icon(
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PreviewPrenotazione(
                          postazione: postazione,
                          oraFineModificabile: true,
                          fine:
                              DateTime.now().toLocal().add(Duration(hours: 2)),
                          inizio: DateTime.now().toLocal(),
                        )));
              },
              icon: Icon(Icons.check, color: Colors.green),
              label: Text('Prenota')),
          TextButton.icon(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(Icons.close, color: Colors.red),
              label: Text('No'))
        ];
        break;
      case StatoPostazioneScan.diNessunoSporca:
        ris = [
          TextButton.icon(
              onPressed: () async {
                await _apiService.pulisci(http.Client(), postazione.rfid);
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PreviewPrenotazione(
                          postazione: postazione,
                          oraFineModificabile: true,
                          fine:
                              DateTime.now().toLocal().add(Duration(hours: 2)),
                          inizio: DateTime.now().toLocal(),
                        )));
              },
              icon: Icon(Icons.check, color: Colors.green),
              label: Text('Pulisci e prenota')),
          TextButton.icon(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(Icons.close, color: Colors.red),
              label: Text('No'))
        ];
        break;
      case StatoPostazioneScan.giaPrenotata:
        ris = [
          TextButton.icon(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(Icons.exit_to_app),
              label: Text('Esci'))
        ];
        break;
      case StatoPostazioneScan.miaFine:
        ris = [
          TextButton.icon(
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (context) => DialogNfcPostReadPulizia(
                          postazione: postazione,
                        ));
                gpsManager.stop();
                Navigator.of(context).pop();
              },
              icon: Icon(Icons.check, color: Colors.green),
              label: Text('Interrompo')),
          TextButton.icon(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(Icons.close, color: Colors.red),
              label: Text('Non interrompere'))
        ];
        break;
      case StatoPostazioneScan.miaInizioPulita:
      case StatoPostazioneScan.miaInizioSporca:
        ris = [
          TextButton.icon(
              onPressed: () async {
                String ris = await gpsManager.start(postazione.rfid);
                if (ris != null) {
                  await showDialog(
                      context: context,
                      builder: (context) =>
                          DialogRisultato(risultato: 'Errore nel GPS:\n$ris'));
                  return;
                }
                _apiServiceEmployee.inizioUso(http.Client(), postazione);
                await showDialog(
                    context: context,
                    builder: (context) => DialogRisultato(
                        risultato: 'Inizio uso della postazione registrato.'));
                Navigator.of(context).pop();
              },
              icon: Icon(Icons.check, color: Colors.green),
              label: Text('Si')),
          TextButton.icon(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(Icons.close, color: Colors.red),
              label: Text('No'))
        ];
        break;
      default:
        ris = [
          TextButton.icon(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(Icons.exit_to_app),
              label: Text('Esci'))
        ];
        break;
    }
    return ris;
  }
}
