import 'package:block_covid/api/ApiServiceEmployee.dart';
import 'package:block_covid/extras/dialogs/DialogRisultato.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class DialogNfcPostReadPulizia extends StatelessWidget {
  DialogNfcPostReadPulizia({@required this.postazione});
  final Postazione postazione;
  final ApiServiceEmployee _apiServiceEmployee = ApiServiceEmployee();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Postazione: ' + postazione.name),
      content: Text('La postazione è stata pulita?'),
      actions: [
        TextButton.icon(
            icon: Icon(Icons.check, color: Colors.green),
            label: Text('Si'),
            onPressed: () async {
              await _apiServiceEmployee.fineUso(
                  http.Client(), postazione, true);
              await showDialog(
                  context: context,
                  builder: (context) => DialogRisultato(
                      risultato: 'Checkout e postazione pulita registrata.'));
              Navigator.of(context).pop();
            }),
        TextButton.icon(
            onPressed: () async {
              await _apiServiceEmployee.fineUso(
                  http.Client(), postazione, false);
              await showDialog(
                  context: context,
                  builder: (context) => DialogRisultato(
                      risultato:
                          'Checkout e postazione non pulita registrata.'));
              Navigator.of(context).pop();
            },
            icon: Icon(Icons.close, color: Colors.red),
            label: Text('No'))
      ],
    );
  }
}
