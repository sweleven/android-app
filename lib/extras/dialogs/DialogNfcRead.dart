import 'package:flutter/material.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';

/// Dialog di lettura dell'NFC.
///
/// Una volta letto il tag lo ritorna per essere catturato dal chiamante
/// di questo dialog. Se l'utente tappa fuori dal dialog o annulla la scansione
/// viene ritornato [null].
class DialogNfcRead extends StatefulWidget {
  @override
  _DialogNfcReadState createState() => _DialogNfcReadState();
}

class _DialogNfcReadState extends State<DialogNfcRead> {
  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      title: Text('Leggo NFC..'),
      content: FutureBuilder(
          future: FlutterNfcReader.read(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              Navigator.of(context).pop(snapshot.data.content.substring(4));
              return Text('');
            }
            // Wrappo in un row sennò si bugga il CircularProgressIndicator
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(strokeWidth: 2),
              ],
            );
          }),
      actions: [
        TextButton.icon(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.exit_to_app),
            label: Text('Esci'))
      ],
    );
  }
}
