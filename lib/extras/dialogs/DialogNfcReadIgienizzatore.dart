import 'package:block_covid/model/Stanza.dart';
import 'package:flutter/material.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';

/// Dialog di lettura dell'NFC da parte dell'igienizzatore.
///
/// Ritorna il risultato della lettura
class DialogNfcReadIgienizzatore extends StatelessWidget {
  final Stanza stanza;
  DialogNfcReadIgienizzatore(this.stanza);

  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      key: Key('NfcDialog'),
      title: Text('Scansione Tag NFC della stanza: ' + stanza.name,
          textAlign: TextAlign.center),
      content: FutureBuilder(
        future: FlutterNfcReader.read(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            Navigator.of(context).pop(snapshot.data.content.substring(3));
            return Text('');
          } else
            // Wrappo in un row sennò si bugga il CircularProgressIndicator
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(strokeWidth: 2),
              ],
            );
        },
      ),
      actions: [
        /// Chiude il dialog. La lettura del tag viene fermata da _showNfcDialog, così anche se l'utente
        /// non preme exit si ferma.
        TextButton.icon(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.exit_to_app,
              key: Key('ExitButtonNfcDialog'),
            ),
            label: Text('Exit'))
      ],
    );
  }
}
