import 'package:flutter/material.dart';

/// Dialog richiesta avvenuta con successo per le [POST]
class DialogRispostaValida extends StatelessWidget {
  DialogRispostaValida({@required this.risultato});
  final TextStyle stiletesto =
      TextStyle(fontSize: 18, fontWeight: FontWeight.w300);

  /// Il testo da mostrare nel dialog.
  final String risultato;
  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      title: Icon(Icons.verified, color: Colors.green, size: 50.0),
      content: Text(risultato, style: stiletesto),
      actions: [
        IconButton(
            icon: Icon(
              Icons.done,
              key: Key("success-btn"),
              color: Colors.green,
            ),
            color: Theme.of(context).primaryColor,
            onPressed: () => Navigator.of(context).pop())
      ],
    );
  }
}
