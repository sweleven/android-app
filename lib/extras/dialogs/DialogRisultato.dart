import 'package:flutter/material.dart';

/// Dialog generico con testo e pulsante esci.
class DialogRisultato extends StatelessWidget {
  DialogRisultato({@required this.risultato});

  /// Il testo da mostrare nel dialog.
  final String risultato;
  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      content: Text(risultato),
      actions: [
        IconButton(
            icon: Icon(Icons.exit_to_app),
            color: Theme.of(context).primaryColor,
            onPressed: () => Navigator.of(context).pop())
      ],
    );
  }
}
