import 'package:flutter/material.dart';

/// Dialog del risultato post lettura nfc della stanza.
///
/// Se la lettura avrà avuto successo ritorna un  dialog con la conferma del successo, altrimenti informerà
/// il fallimento
class DialogRisultatoIgienizzatore extends StatelessWidget {
  DialogRisultatoIgienizzatore(
      {@required this.risultato, @required this.verificato});

  /// Il testo da mostrare nel dialog.
  final String risultato;
  final bool verificato;
  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Risultato scansione',
        textAlign: TextAlign.center,
      ),
      elevation: 10,
      content: Text(
        risultato,
        textAlign: TextAlign.center,
      ),
      actions: [
        TextButton.icon(
            key: Key('verificato'),
            label: verificato
                ? Text('Esci', style: TextStyle(color: Colors.green))
                : Text(
                    'Esci',
                    style: TextStyle(color: Colors.red),
                  ),
            icon: verificato
                ? Icon(
                    Icons.verified,
                    color: Colors.green,
                  )
                : Icon(
                    Icons.cancel,
                    color: Colors.red,
                  ),
            onPressed: () => Navigator.of(context).pop())
      ],
    );
  }
}
