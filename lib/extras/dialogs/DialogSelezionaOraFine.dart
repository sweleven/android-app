import 'package:flutter/material.dart';
import 'package:block_covid/extras/dialogs/DialogErrore.dart';

/// widget Finestra di dialogo che ritorna l'orario finale in base all' orario iniziale inserito tramite l'iterazione del utente
/// richiede un ora iniziale ed una lista
class DialogSelezionaOraFine extends StatefulWidget {
  final TimeOfDay start = TimeOfDay(hour: 8, minute: 0);
  final TimeOfDay end = TimeOfDay(hour: 20, minute: 0);
  final DateTime passato;
  final List<String> nameList = List<String>.empty(
      growable: true); //sentinella per capire se l'ora passata è valida
  DialogSelezionaOraFine(DateTime inizio) : passato = inizio {
    if (nameList != null) nameList.clear();
    //PRE orario inizio valido
    int indicefinale = end.hour - inizio.hour;
    if (inizio.minute > 0) indicefinale = indicefinale;
    for (int i = 1; i <= indicefinale; i++) {
      if (i == (indicefinale))
        nameList.add('resto della giornata');
      else
        nameList.add('$i ore');
    }
    //for (var i = 0; i < nameList.length; i++) print(nameList.elementAt(i));
  }
  DateTime getOrarioFine(int indice) {
    if (indice < nameList.length - 1)
      return DateTime(passato.year, passato.month, passato.day,
          (passato.hour) + (indice + 1), passato.minute);
    else
      return DateTime(
          passato.year, passato.month, passato.day, end.hour, end.minute);
  }

  @override
  DialogSelezionaOraFineState createState() => DialogSelezionaOraFineState();
}

class DialogSelezionaOraFineState extends State<DialogSelezionaOraFine> {
  /*@override
  void initState() {
    super.initState();
    widget.timenotvalid = widget.orarioInizioisValid(widget.passato);
  }*/
  final ScrollController _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    if (widget.passato.hour < widget.start.hour ||
        widget.passato.hour > widget.end.hour ||
        (widget.passato.hour == widget.end.hour &&
            widget.passato.minute > widget.end.minute))
      return DialogErrore(risultato: 'orario iniziale non valido');
    else
      return AlertDialog(
          key: Key("dialog-durata"),
          title: Text('Seleziona durata:'),
          content: Container(
              height: 300,
              width: 250,
              padding: EdgeInsets.all(10.0),
              decoration: new BoxDecoration(color: const Color(000000)),
              child: Column(
                children: <Widget>[
                  Container(
                      decoration: new BoxDecoration(
                          border: Border.all(
                              width: 1.0, color: Colors.grey.shade300),
                          borderRadius: new BorderRadius.only(
                              topLeft: Radius.circular(16.0),
                              bottomLeft: Radius.circular(16.0))),
                      height: 256,
                      child: Scrollbar(
                        controller: _scrollController,
                        isAlwaysShown: true,
                        thickness: 12,
                        interactive: true,
                        child: ListView.builder(
                            controller: _scrollController,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                key: Key("durata"),
                                behavior: HitTestBehavior.translucent,
                                child: Container(
                                    margin:
                                        EdgeInsets.fromLTRB(10, 10, 25.0, 10),
                                    decoration: new BoxDecoration(
                                        border: Border.all(
                                            width: 1.0,
                                            color: Colors.transparent),
                                        borderRadius:
                                            new BorderRadius.circular(16.0),
                                        color: Colors.grey.shade200),
                                    padding: new EdgeInsets.fromLTRB(
                                        6.0, 6.0, 6.0, 6.0),
                                    child: Text('${widget.nameList[index]}',
                                        key: Key("btn-durata"),
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center)),
                                onTap: () => Navigator.of(context)
                                    .pop(widget.getOrarioFine(index)),
                              );
                            },
                            itemCount: widget.nameList.length),
                      ))
                ],
              )));
  }
}
