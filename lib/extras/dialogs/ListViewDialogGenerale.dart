// ignore_for_file: unused_local_variable

import 'package:block_covid/model/Postazione.dart';
import 'package:block_covid/model/Stanza.dart';
import 'package:flutter/material.dart';

/// widget Finestra di dialogo che ritorna l'elemento selezionato dal utente(se selezionato),
/// il modello della [listaElementi] deve avere un campo dati chiamato [name]
/// (ex.utilizzare per lista di stanze o postazioni(nel caso si voglia leggere il campo dati [clean] per far vedere la leggenda bisogna mettere a true [dirty]))
// ignore: must_be_immutable
class ListViewDialogGenerale extends StatefulWidget {
  final String titleDialog;
  final List<dynamic> nameList;
  final bool dirty;
  List<dynamic> activelist; //cambia nel tempo
  final List<dynamic> cleanlist = [];
  final List<dynamic> dirtylist = [];
  ListViewDialogGenerale(String titolo, List<dynamic> listaElementi,
      {bool dirty = false})
      : nameList = listaElementi,
        titleDialog = titolo,
        this.dirty = dirty {
    activelist = listaElementi;
    _buildLists(listaElementi);
  }
  void _buildLists(List<dynamic> listaElementi) {
    if (listaElementi.isNotEmpty && listaElementi.first is Postazione)
      for (int i = 0; i != listaElementi.length; i++)
        if (listaElementi[i].clean == true)
          cleanlist.add(listaElementi[i]);
        else
          dirtylist.add(listaElementi[i]);
  }

  @override
  ListViewDialogGeneraleState createState() => ListViewDialogGeneraleState();
}

class ListViewDialogGeneraleState extends State<ListViewDialogGenerale> {
  static const legendsqaureswidth = 10.0;
  static double fontsizelistviewitem = 20.0;
  TextStyle bold = TextStyle(
    fontWeight: FontWeight.bold,
    /*per il requisito opzionale colorare le varie postazioni in base allo stato*/
  );
  TextStyle postazionipulite = TextStyle(
    fontWeight: FontWeight.bold,
  );
  TextStyle normale = TextStyle(fontWeight: FontWeight.bold);
  static const Color bottonipulita = Colors.lightBlueAccent;
  static const Color bottonisporca = Colors.yellowAccent;
  //END STYLE;
  bool active;
  bool activedclean;
  @override
  void initState() {
    super.initState();
    active = false;
    activedclean = true;
  }

  //end sizedBox
  final ScrollController _scrollController = ScrollController();
  @override
  AlertDialog build(BuildContext context) {
    double _resizedheight = 430;
    double _height = 290;
    if (widget.dirty != false) _height = _resizedheight;
    if (widget.activelist == null || widget.activelist.isEmpty)
      return AlertDialog(
          title: Text('Al momento non c\'è nessuna ' +
              widget.titleDialog +
              ' disponibile'),
          content: Container(
            width: 100.0,
            height: 100.0,
          ));
    else
      return AlertDialog(
          key: Key("LVDG"),
          title: Text('Seleziona ' + widget.titleDialog + ':'),
          content: Container(
              height: _height,
              width: 290,
              padding: EdgeInsets.all(10.0),
              decoration: new BoxDecoration(color: const Color(000000)),
              child: Column(
                children: <Widget>[
                  if (widget.dirty == true)
                    Container(
                        margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                        child: Column(
                          children: <Widget>[
                            if ((widget.activelist == null ||
                                    widget.activelist.isEmpty) &&
                                widget.dirty == false)
                              AlertDialog(
                                  title: Text('Al momento non c\'è nessuna ' +
                                      widget.titleDialog +
                                      ' disponibile'),
                                  content: Container(
                                    width: 100.0,
                                    height: 100.0,
                                  )),
                            if ((widget.activelist == null ||
                                    widget.activelist.isEmpty) &&
                                widget.dirty)
                              AlertDialog(
                                  title: Text('Al momento non c\'è nessuna ' +
                                      widget.titleDialog +
                                      ' disponibile'),
                                  content: Container(
                                    width: 100.0,
                                    height: 120.0,
                                  )),
                            if (widget.activelist.isNotEmpty && widget.dirty)
                              Row(children: <Widget>[
                                Text('legenda: ',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                SizedBox(
                                    width: legendsqaureswidth,
                                    height: legendsqaureswidth,
                                    child: const DecoratedBox(
                                      decoration: const BoxDecoration(
                                          color: bottonisporca),
                                    )),
                                Text(' Sporca '),
                                SizedBox(
                                    width: legendsqaureswidth,
                                    height: legendsqaureswidth,
                                    child: const DecoratedBox(
                                      decoration: const BoxDecoration(
                                          color: bottonipulita),
                                    )),
                                Text(' Pulita '),
                              ]),
                            if (widget.cleanlist.isNotEmpty &&
                                widget.dirtylist.isNotEmpty)
                              Row(
                                children: <Widget>[
                                  Icon(Icons.filter_alt),
                                  Padding(
                                      padding: EdgeInsets.all(10),
                                      child: TextButton(
                                          style: TextButton.styleFrom(
                                            primary: Colors.white,
                                            backgroundColor: Colors.red,
                                          ),
                                          onPressed: () async {
                                            if (!active) {
                                              _switchCleanList();
                                              active = !active;
                                            }
                                          },
                                          child: Text('Pulite'))),
                                  Padding(
                                      padding: EdgeInsets.all(10),
                                      child: TextButton(
                                          style: TextButton.styleFrom(
                                            primary: Colors.white,
                                            backgroundColor: Colors.red,
                                          ),
                                          onPressed: () async {
                                            if (activedclean) {
                                              _switchCompleteList();
                                              active = !active;
                                            }
                                          },
                                          child: Text('Tutte')))
                                ],
                              ),
                            if (widget.dirtylist.isEmpty)
                              Column(children: [
                                Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Image(
                                        image:
                                            AssetImage('lib/assets/Lindo.png'),
                                        width: 99,
                                        height: 99)),
                                Text(
                                    'Tutte le postazioni attualmente libere sono pulite!')
                              ])
                          ],
                        )),
                  if (widget.activelist.isNotEmpty)
                    Container(
                        decoration: new BoxDecoration(
                          border: Border.all(
                              width: 1.0, color: Colors.grey.shade200),
                          borderRadius: new BorderRadius.only(
                              topLeft: Radius.circular(16.0),
                              bottomLeft: Radius.circular(16.0)),
                          color: Colors.white60,
                        ),
                        height: 226,
                        child: Scrollbar(
                          controller: _scrollController,
                          isAlwaysShown: true,
                          thickness: 12,
                          interactive: true,
                          child: ListView.builder(
                              controller: _scrollController,
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: tastoMenu(index, Colors.grey.shade200),
                                  onTap: () => Navigator.of(context)
                                      .pop(widget.activelist[index]),
                                );
                              },
                              itemCount: widget.activelist.length),
                        ))
                ],
              )));
  }

  Text _isPostazioneSporcaOPulita(index) {
    if (widget.activelist.first is Postazione) {
      //if(widget.nameList[index].pulita==true)
      return Text('${widget.activelist[index].name}',
          style: bold, textAlign: TextAlign.center);
      //else
      //return Text('${widget.nameList[index].name}',style: postazionispor,textAlign: TextAlign.center);
    }
    return Text('${widget.activelist[index].name}',
        style: normale, textAlign: TextAlign.center);
  }

  Container tastoMenu(index, Color colore) {
    Icon icona;

    if (widget.activelist.first is Postazione) icona = Icon(Icons.computer);
    if (widget.activelist.first is Stanza) icona = Icon(Icons.sensor_door);
    return Container(
        key: Key("LVDG-button"),
        margin: EdgeInsets.fromLTRB(6, 6, 25, 6),
        decoration: new BoxDecoration(
          borderRadius: new BorderRadius.circular(16.0),
          color: _coloretasto(index, colore),
        ),
        padding: new EdgeInsets.all(13.0),
        child: Center(
            child: Row(
                children: <Widget>[icona, _isPostazioneSporcaOPulita(index)])));
  }

  void _switchCleanList() {
    setState(() {
      widget.activelist = widget.cleanlist;
    });
  }

  void _switchCompleteList() {
    setState(() {
      widget.activelist = widget.nameList;
    });
  }

  Color _coloretasto(index, colore) {
    if (widget.dirty) {
      if (widget.activelist[index].clean == true)
        return bottonipulita;
      else
        return bottonisporca;
      //altrimenti colore pulita
    } else
      return colore;
  }
}
