import 'package:block_covid/pages/Login.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(BlockCovidApp());
}

class BlockCovidApp extends StatelessWidget {
  final ThemeData kDefaultTheme = ThemeData(
      primarySwatch: Colors.red,
      primaryColor: Colors.red[500],
      accentColor: Colors.orange[300],
      brightness: Brightness.light);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'BlockCOVID',
      theme: kDefaultTheme,
      home: Login(),
    );
  }
}
