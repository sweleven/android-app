import 'package:background_location/background_location.dart';
import 'package:block_covid/api/ApiServiceEmployee.dart';
import 'package:block_covid/model/NotificheManager.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:permission_handler/permission_handler.dart';

/// Gestisce l'attivazione del GPS e il tracciamento in background.
///
/// Mostra una notifica se l'utente esce dal recinto geografico stabilito all'avvio del tracciamento.
class GpsManager {
  NotificheManager _notificheManager;
  double _startLatitude;
  double _startLongitude;
  bool _started = false;
  // Tempo tra una scansione e l'altra in ms (in verità poi fa come gli pare)
  int _timing = 5000;
  bool _timerPartito = false;
  String _rfidPostazione;

  /// Costruttore principale.
  ///
  /// [callBack] è cosa deve eseguire l'app quando l'utente tappa sulla notifica. Può anche non fare niente.
  GpsManager({@required Future<dynamic> Function(String) callBack}) {
    _notificheManager = NotificheManager(callBack: callBack);
  }

  /// Avvia il rilevamento della posizione in background.
  ///
  /// Richiede come parametro la postazione che sta tracciando.
  ///
  /// Se ci sono errori come permessi o ecc li ritorna come [String], altrimenti ritorna [null].
  Future<String> start(String rfid) async {
    _rfidPostazione = rfid;
    if (!await Permission.locationAlways.request().isGranted) {
      return 'Permessi di geolocalizzazione non concessi.';
    }
    await BackgroundLocation.setAndroidNotification(
      title: 'GPS in uso',
      message: 'È in corso la localizzazione in background',
      icon: '@mipmap/ic_launcher',
    );
    await BackgroundLocation.setAndroidConfiguration(_timing);
    BackgroundLocation.getLocationUpdates(_updateRicevuto);
    await BackgroundLocation.startLocationService(distanceFilter: 20);
    return null;
  }

  /// Funzione attivata quando scade il timer.
  void _timerScaduto() async {
    await ApiServiceEmployee().fineUso(
        http.Client(),
        Postazione(
            clean: false, rfid: _rfidPostazione, id: '', name: '', room: ''),
        false);
    this.stop();
  }

  /// Funzione chiamata ogni volta che il listener riceve una nuova posizione.
  ///
  /// Si occupa di aggiornare la posizione attuale e di mostrare una notifica se esce dal recinto geografico.
  ///
  /// Cancella la notifica se si è dentro il recinto geografico
  void _updateRicevuto(Location location) async {
    print(location.latitude.toString() + ' ' + location.longitude.toString());
    if (location.accuracy > 20) {
      return;
    }
    if (!_started) {
      _startLatitude = location.latitude;
      _startLongitude = location.longitude;
      _started = true;
      return;
    }
    double deltaPosizione = (_startLatitude - location.latitude).abs() +
        (_startLongitude - location.longitude).abs();
    print('delta = ' + deltaPosizione.toString());
    if (deltaPosizione > 0.0002) {
      List<ActiveNotification> activeNotifications =
          await _notificheManager.getActiveNotifications();
      if (activeNotifications.length <= 1) {
        // Se c'è gia la notifica del recinto geografico non ne manda un altra.
        await _notificheManager.mostraNotifica(
            titolo: 'Recinto geografico',
            body:
                'Si sta uscendo dal recinto geografico della postazione. Ritornarci o la prenotazione sarà terminata.');
        if (!_timerPartito) {
          _timerPartito = true;
          Timer(Duration(minutes: 15), _timerScaduto);
        }
      }
    } else {
      // Se rientra nel recinto cancella la notifica mandata.
      _notificheManager.cancellaNotifiche();
    }
  }

  /// Interrompe il tracciamento in background col GPS.
  void stop() {
    BackgroundLocation.stopLocationService();
    _started = false;
    _notificheManager.cancellaNotifiche();
  }
}
