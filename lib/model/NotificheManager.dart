import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:meta/meta.dart';

/// Gestisce la creazione, comparsa e distruzione delle notifiche.
class NotificheManager {
  FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;
  NotificationDetails _platformChannelSpecifics;

  /// Costruttore principale.
  ///
  /// [callBack] è cosa deve eseguire l'app quando l'utente tappa sulla notifica. Può anche non fare niente.
  NotificheManager({@required Future<dynamic> Function(String) callBack}) {
    _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    _platformChannelSpecifics = NotificationDetails(
        android: AndroidNotificationDetails('id1', 'name1', 'description1',
            importance: Importance.max,
            priority: Priority.high,
            ticker: 'ticker',
            playSound: true));
    InitializationSettings _initializationSettings = InitializationSettings(
        android: AndroidInitializationSettings('@mipmap/ic_launcher'),
        iOS: IOSInitializationSettings());
    _flutterLocalNotificationsPlugin.initialize(_initializationSettings,
        onSelectNotification: callBack);
  }

  Future<List<ActiveNotification>> getActiveNotifications() async {
    return await _flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        .getActiveNotifications();
  }

  Future<void> mostraNotifica(
      {@required String titolo, @required String body}) async {
    await _flutterLocalNotificationsPlugin.show(
        0, titolo, body, _platformChannelSpecifics);
  }

  void cancellaNotifiche() {
    _flutterLocalNotificationsPlugin.cancelAll();
  }
}
