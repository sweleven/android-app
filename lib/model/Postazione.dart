import 'package:block_covid/model/StatoPostazioneScan.dart';
import 'package:meta/meta.dart';

/// Rappresenta la postazione, intesa come stanza o computer singolo
class Postazione {
  Postazione(
      {@required this.id,
      @required this.name,
      @required this.room,
      @required this.rfid,
      @required this.clean,
      this.stato});

  /// ID della postazione
  final String id;
  final String name;
  final String room;
  final String rfid;
  bool clean;
  StatoPostazioneScan stato;

  factory Postazione.fromJson(Map<String, dynamic> json) {
    return Postazione(
        id: json['_id'],
        room: json['room'],
        name: json['name'],
        rfid: json['rfid'],
        clean: json['clean']);
  }
}
