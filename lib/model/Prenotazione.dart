import 'package:block_covid/model/Postazione.dart';
import 'package:meta/meta.dart';

/// Rappresenta la prenotazione.
class Prenotazione {
  Prenotazione(
      {this.id = '',
      @required this.postazione,
      @required this.inizio,
      @required this.fine,
      this.consumed,
      this.isComputer = true,
      this.note = '',
      this.cancellazione});

  /// L'id della prenotazione.
  final String id;

  /// La postazione (stanza/computer) in oggetto.
  final Postazione postazione;

  /// Inizio della prenotazione.
  final DateTime inizio;

  /// Fine della prenotazione
  final DateTime fine;

  /// Indica se la prenotazione si riferisce ad un
  /// computer singolo o ad una stanza intera
  final bool isComputer;

  /// Note aggiuntive alla prenotazione
  final String note;

  /// Data di cancellazione se esistente
  final DateTime cancellazione;

  /// Indica se è stata attivata col check-in
  final bool consumed;

  factory Prenotazione.fromJson(Map<String, dynamic> json) {
    return Prenotazione(
        id: json['_id'],
        postazione: json['workspace'],
        inizio: json['start_datetime'],
        fine: json['end_datetime']);
  }
}
