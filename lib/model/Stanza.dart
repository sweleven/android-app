import 'package:block_covid/model/Postazione.dart';
import 'package:meta/meta.dart';

/// Rappresenta la stanza intesa come insieme di postazioni
class Stanza {
  /* {
    "_id": "6069caf5fad3c40029f651fe",
    "name": "2",
    "__v": 0,
    "workspaces": [
      {
        "_id": "6069cb15fad3c40029f651ff",
        "room": "6069caf5fad3c40029f651fe",
        "name": "1",
        "rfid": "rfidCODE",
        "__v": 0
      }
    ],
    "workspaces_total": 1
  }*/
  Stanza(
      {@required this.id,
      @required this.name,
      @required this.v,
      @required this.rfid,
      @required this.listapostazioni,
      @required this.totalepostazioni});

  /// ID della stanza
  final String id;

  /// Nome della stanza
  final String name;

  final int v;

  /// Rfid della stanza
  final String rfid;

  /// Lista delle postazioni interne alla stanza
  final List<Postazione> listapostazioni;
  final int totalepostazioni;

  factory Stanza.fromJson(Map<String, dynamic> json) {
    var postazioni = json['workspaces'] as List;
    List<Postazione> postazioniList =
        postazioni.map((i) => Postazione.fromJson(i)).toList();
    return Stanza(
        id: json['_id'],
        name: json['name'],
        v: json['__v'],
        rfid: json['rfid'],
        listapostazioni: postazioniList,
        totalepostazioni: json['workspaces_total']);
  }
}
