/// Indica lo stato della prenotazione scannerizzata con
/// l'nfc da parte di un dipendente.
///
/// [giaPrenotata]: la postazione è già prenotata;
///
/// [miaInizioPulita]: la postazione è prenotata da me ed è la prima volta che la scannerizzo ed è pulita;
///
/// [miaInizioSporca]: la postazione è prenotata da me ed è la prima volta che la scannerizzo ed è sporca;
///
/// [miaFine]: la postazione è prenotata da me ed e la seconda o più volta che la scannerizzo;
///
/// [diNessunoSporca]: la postazione scannerizzata non appartiene a nessuno ed è sporca;
///
/// [diNessunoPulita]: la postazione scannerizzata non appartiene a nessuno ed è pulita.
enum StatoPostazioneScan {
  giaPrenotata,
  miaInizioPulita,
  miaInizioSporca,
  miaFine,
  diNessunoSporca,
  diNessunoPulita,
}
