// ignore_for_file: unused_local_variable
//import 'package:block_covid/extras/dialogs/DialogRisultato.dart';
import 'package:block_covid/api/ApiServiceEmployee.dart';
import 'package:block_covid/extras/dialogs/ListViewDialogGenerale.dart';
import 'package:flutter/material.dart';
import 'package:block_covid/api/ApiService.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:block_covid/model/Prenotazione.dart';
//import 'package:block_covid/extras/model/CreaPrenotaModel.dart';
import 'package:block_covid/extras/dialogs/DialogErrore.dart';
import 'package:block_covid/model/Stanza.dart';
import 'package:flutter/cupertino.dart';
import 'package:block_covid/extras/dialogs/DialogSelezionaOraFine.dart';
import 'package:block_covid/extras/dialogs/DialogConfermaPrenotazione.dart';
import 'package:http/http.dart' as http;

///CreaPrenotazione è il widget che permette di far settare i campi dati all'utente tramite la UI e fare una richiesta di prenotazione
class CreaPrenotazione extends StatefulWidget {
  ///campi dati per fare controlli sul orario permesso
  final ApiService _apiService = ApiService();
  final ApiServiceEmployee _apiServiceEmployee = ApiServiceEmployee();
  @override
  CreaPrenotazioneState createState() => CreaPrenotazioneState();
}

class CreaPrenotazioneState extends State<CreaPrenotazione> {
  final _dateController = TextEditingController();
  final _timeController = TextEditingController();
  final _timeController1 = TextEditingController();
  final _postazioneController = TextEditingController();
  final _stanzaController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  //costanti di supporto da definire in un file opportuno
  TimeOfDay start = TimeOfDay(hour: 8, minute: 0);
  TimeOfDay end = TimeOfDay(hour: 20, minute: 0);

  bool sent = true; //segnalare errori

  /// Mi serve per chiedere al server quali sono le postazioni disponibili.
  DateTime _dataSelezionata;
  DateTime _orarioInizio;
  DateTime _orarioFine;
  //variabili di supporto
  TimeOfDay _orarioiniziale;
  TimeOfDay _orariodifine;
  List<Stanza> _stanzeconPostazioniPrenotabili = <Stanza>[];
  Stanza _stanzaselezionata;
  Postazione _postazioneselezionata;
  //style
  TextStyle fontforms = TextStyle(fontSize: 18.0, color: Colors.black);
  TextStyle buttonstyle =
      TextStyle(fontSize: 18, color: Colors.lightGreenAccent);
  @override
  Widget build(BuildContext context) {
    return Container(
      key: Key("CreaPrenotazione"),
      margin: EdgeInsets.all(18),
      padding: EdgeInsets.all(0),
      child: Form(
          key: _formKey,
          child: ListView(
            children: [
              Container(
                  margin: EdgeInsets.all(5),
                  child: TextFormField(
                    key: Key("data"),
                    style: fontforms,
                    validator: (_dataSelezionata) {
                      if (_dataSelezionata == null ||
                          _dataSelezionata.isEmpty) {
                        return 'Data non selezionata ';
                      }
                      return null;
                    },
                    autofocus: false,
                    readOnly: true,
                    controller: _dateController,
                    decoration: InputDecoration(
                      hintText: 'Seleziona la data',
                      icon: Icon(Icons.calendar_today_outlined),
                      //contentPadding: EdgeInsets.all(10),
                    ),
                    onTap: () async {
                      _selectDate();
                    },
                  )),
              Container(
                  margin: EdgeInsets.all(5),
                  child: TextFormField(
                    key: Key("orainizio"),
                    style: fontforms,
                    //cambiare controller ed instanzialrlo se orario anche di fine
                    validator: (_orarioInizio) {
                      if (_orarioInizio == null || _orarioInizio.isEmpty) {
                        return 'Orario iniziale non selezionato';
                      }
                      return null;
                    },
                    autofocus: false,
                    readOnly: true,
                    controller: _timeController,
                    decoration: InputDecoration(
                      hintText: 'Seleziona orario di inizio',
                      icon: Icon(Icons.timer),
                    ),
                    onTap: () async {
                      if (_dataSelezionata != null)
                        _selectTime();
                      else {
                        await showDialog(
                            context: context,
                            builder: (context) => DialogErrore(
                                risultato: "Impostare una data prima"));
                      }
                    },
                  )),
              Container(
                  key: Key("durata"),
                  margin: EdgeInsets.all(5),
                  child: TextFormField(
                      style: fontforms,
                      validator: (_orarioFine) {
                        if (_orarioFine == null || _orarioFine.isEmpty) {
                          return 'Durata non selezionata';
                        }
                        return null;
                      },
                      autofocus: false,
                      readOnly: true,
                      controller: _timeController1,
                      decoration: InputDecoration(
                        hintText: 'Seleziona durata',
                        icon: Icon(Icons.pending_actions),
                      ),
                      onTap: () async {
                        if (_orarioInizio != null) {
                          var a = await showDialog(
                              context: context,
                              builder: (context) =>
                                  DialogSelezionaOraFine(_orarioInizio));
                          if (a != null)
                            setState(() {
                              _orarioFine = a;
                              _orariodifine =
                                  TimeOfDay.fromDateTime(_orarioFine);
                              _timeController1.text =
                                  TimeOfDay.fromDateTime(_orarioFine)
                                      .format(context);
                            });
                        } else {
                          await showDialog(
                              context: context,
                              builder: (context) => DialogErrore(
                                  risultato:
                                      "Impostare una data ed un orario iniziale prima"));
                        }
                      })),
              Container(
                  margin: EdgeInsets.all(5),
                  child: TextFormField(
                      key: Key("selstanza"),
                      style: fontforms,
                      validator: (_stanzaselezionata) {
                        if (_stanzaselezionata == null ||
                            _stanzaselezionata.isEmpty) {
                          return 'Selezionare una stanza';
                        }
                        return null;
                      },
                      autofocus: false,
                      readOnly: true,
                      controller: _stanzaController, //_stanzaController,
                      decoration: InputDecoration(
                        hintText: 'Seleziona una stanza',
                        icon: Icon(Icons.sensor_door),
                      ),
                      onTap: () async {
                        if (_dataSelezionata != null &&
                            _orarioInizio != null &&
                            _orarioFine != null) {
                          _stanzeconPostazioniPrenotabili =
                              await widget._apiService.getRooms(http.Client());
                          Stanza a = await showDialog(
                              context: context,
                              builder: (context) => ListViewDialogGenerale(
                                  "stanza", _stanzeconPostazioniPrenotabili));

                          if (a != null &&
                              _stanzeconPostazioniPrenotabili != null)
                            setState(() {
                              if (_stanzaselezionata != null &&
                                  _stanzaselezionata != a) {
                                _postazioneselezionata = null;
                                _postazioneController.text = "";
                              }
                              _stanzaselezionata = a;
                              _stanzaController.text = _stanzaselezionata.name;
                            });
                        } else {
                          await showDialog(
                              context: context,
                              builder: (context) => DialogErrore(
                                  risultato:
                                      "Impostare i campi dati precendenti prima"));
                        }
                      })),
              Container(
                  margin: EdgeInsets.all(5),
                  child: TextFormField(
                      key: Key("selpostazione"),
                      style: fontforms,
                      //cambiare controller ed instanzialrlo se orario anche di fine
                      autofocus: false,
                      readOnly: true,
                      validator: (_postazioneselezionata) {
                        if (_postazioneselezionata == null ||
                            _postazioneselezionata.isEmpty) {
                          return 'Selezionare una postazione';
                        }
                        return null;
                      },
                      controller: _postazioneController, //_stanzaController,
                      decoration: InputDecoration(
                        hintText: 'Seleziona una postazione',
                        icon: Icon(Icons.computer),
                      ),
                      onTap: () async {
                        if (_stanzaselezionata != null) {
                          List<Postazione> workspacesDisponibili = await widget
                              ._apiServiceEmployee
                              .postazioniDisponibili(
                                  http.Client(),
                                  _stanzaselezionata.id,
                                  _orarioInizio,
                                  _orarioFine);
                          Postazione a = await showDialog(
                              context: context,
                              builder: (context) => _orarioInizio.year ==
                                          DateTime.now().year &&
                                      _orarioInizio.month ==
                                          DateTime.now().month &&
                                      _orarioInizio.day == DateTime.now().day
                                  ? ListViewDialogGenerale(
                                      "postazione", workspacesDisponibili,
                                      dirty: true)
                                  : ListViewDialogGenerale(
                                      "postazione", workspacesDisponibili));

                          if (a != null && _stanzaselezionata != null)
                            setState(() {
                              _postazioneselezionata = a;
                              _postazioneController.text =
                                  _postazioneselezionata.name;
                            });
                        } else {
                          await showDialog(
                              context: context,
                              builder: (context) => DialogErrore(
                                  risultato: "Selezionare una stanza prima"));
                        }
                      })),
              Center(
                child: ElevatedButton(
                    key: Key("bottoneprenota"),
                    style: ElevatedButton.styleFrom(textStyle: buttonstyle),
                    onPressed: () async {
                      if (checkFormisempty() == true) {
                        await showDialog(
                            context: context,
                            builder: (context) => DialogErrore(
                                risultato:
                                    "Alcuni campi dati non sono stati selezionati, controllare e riprovare"));
                      }
                      if (_formKey.currentState.validate()) {
                        await showDialog(
                            context: context,
                            builder: (context) => DialogConfermaPrenotazione(
                                prenotazione: Prenotazione(
                                    postazione: _postazioneselezionata,
                                    inizio: _orarioInizio,
                                    fine: _orarioFine)));
                      }
                    },
                    child: Text('Prenota')),
              )
            ],
          )),
    );
  }

  @override
  void dispose() {
    _dateController.dispose();
    _timeController.dispose();
    _timeController1.dispose();
    _postazioneController.dispose();
    _stanzaController.dispose();
    super.dispose();
  }

  /// Mostra il dialog per selezionare la data.
  /// Aggiorna la variabile _dataSelezionata se corretta
  /// Aggiorna le variabili OrarioFine e inizio se già settate
  Future<DateTime> _selectDate() async {
    TimeOfDay end = TimeOfDay(hour: 20, minute: 0);
    DateTime domani = DateTime.now().add(Duration(days: 1));
    //inizializza a domani la data se hai superato l'end da metterle come costanti da qualche parte
    DateTime ora = (DateTime.now().hour > end.hour ||
            (DateTime.now().hour == end.hour &&
                DateTime.now().minute > end.minute))
        ? domani
        : DateTime.now();
    DateTime ris = await showDatePicker(
        context: context,
        initialDate: ora,
        firstDate: ora,
        lastDate: DateTime.now().add(const Duration(days: 35)),
        helpText: 'Seleziona la data',
        cancelText: 'Cancella',
        errorFormatText: 'Data in formato non corretto',
        errorInvalidText: 'Data non valida',
        fieldHintText: 'Inserisci la data',
        fieldLabelText: 'Inserisci la data');

    if (ris != null) {
      _dataSelezionata = ris;
      _dateController.text = _dataSelezionata.toString().substring(0, 10);
      if (_orarioiniziale != null)
        _orarioInizio = DateTime(_dataSelezionata.year, _dataSelezionata.month,
            _dataSelezionata.day, _orarioiniziale.hour, _orarioiniziale.minute);
      if (_orariodifine != null)
        _orarioFine = DateTime(_dataSelezionata.year, _dataSelezionata.month,
            _dataSelezionata.day, _orariodifine.hour, _orariodifine.minute);
      if (_stanzaselezionata != null) {
        resetStanzaePostazione();
      }

      setState(() {
        // Fare get dal server per ottenere le postazioni disponibili
      });
    } else {
      await showDialog(
          context: context,
          builder: (context) =>
              DialogErrore(risultato: 'Data selezionata non valida'));
      return null;
    }
    return null;
  }

  ///ritorna true se un campo dati della form non è stato settato altrimenti ritorna false
  bool checkFormisempty() {
    if (_dataSelezionata == null ||
        _orarioInizio == null ||
        _orarioFine == null ||
        _stanzaselezionata == null ||
        _postazioneselezionata == null)
      sent = true;
    else
      sent = false;
    return sent;
  }

  void resetStanzaePostazione() {
    if (_stanzaselezionata != null) {
      _stanzaController.text = '';
      _stanzaselezionata = null;
    }
    if (_stanzaselezionata == null) {
      _postazioneController.text = '';
      _postazioneselezionata = null;
    }
  }

  /// Mostra il dialog per selezionare l'ora di inizio
  /// Aggiorna la variabile _orarioinizio se orario impostato e nel range >= start e < end
  Future<DateTime> _selectTime() async {
    TimeOfDay start = TimeOfDay(hour: 8, minute: 0);
    TimeOfDay end = TimeOfDay(hour: 20, minute: 0);
    DateTime now = DateTime.now();
    TimeOfDay ora = (DateTime.now().hour > end.hour ||
            DateTime.now().hour < start.hour ||
            (DateTime.now().hour == end.hour &&
                DateTime.now().minute > end.minute))
        ? start
        : TimeOfDay.now();
    TimeOfDay orarioscelto = await showTimePicker(
        initialTime: ora,
        context: context,
        helpText: 'Seleziona ora di inizio',
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
              data:
                  MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
              child: child);
        });
    if (orarioscelto != null &&
        orarioscelto.hour >= start.hour &&
        orarioscelto.hour < end.hour) {
      //controllo per vedere se l'orario ha senso nel caso di orario odierno
      if (((now.year == _dataSelezionata.year &&
                  now.month == _dataSelezionata.month &&
                  now.day == _dataSelezionata.day) &&
              (now.hour > orarioscelto.hour ||
                  (now.hour == orarioscelto.hour) &&
                      now.minute > orarioscelto.minute)) ||
          (orarioscelto.hour == end.hour && orarioscelto.minute > end.minute)) {
        await showDialog(
            context: context,
            builder: (context) =>
                DialogErrore(risultato: 'Ora selezionata non valida'));
        return null;
      }
      //ora valida
      setState(() {
        //se cambio l'ora d'inizio ha senso resettere quella di fine e la stanza perchè potrebbe essere non disponibile
        //una cosa piu smart sarebbe fare la richiesta all'API se quella postazione è free nel orario settato prima di resetStanzaePostazione
        if (_orarioFine != null) {
          _orarioFine = null;
          _orariodifine = null;
          _timeController1.text = "";
          resetStanzaePostazione();
        } //differenza ore=durata

        _orarioInizio = DateTime(_dataSelezionata.year, _dataSelezionata.month,
            _dataSelezionata.day, orarioscelto.hour, orarioscelto.minute);
        _orarioiniziale = orarioscelto;
        _timeController.text = orarioscelto.format(context);
      });
    } else {
      await showDialog(
          context: context,
          builder: (context) =>
              DialogErrore(risultato: 'Ora selezionata non valida'));
      return null;
    }
    return null;
  }
}
