import 'package:block_covid/extras/cards/CardDoppia.dart';
import 'package:block_covid/extras/dialogs/DialogDeletePrenotazioe.dart';
import 'package:block_covid/extras/dialogs/DialogRisultato.dart';
import 'package:block_covid/model/Prenotazione.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DettaglioPrenotazione extends StatefulWidget {
  DettaglioPrenotazione(
      {@required this.prenotazione, @required this.eliminabile});
  final Prenotazione prenotazione;
  final bool eliminabile;

  @override
  _DettaglioPrenotazioneState createState() => _DettaglioPrenotazioneState();
}

class _DettaglioPrenotazioneState extends State<DettaglioPrenotazione> {
  final TextStyle _stileMain =
      TextStyle(fontSize: 17.0, fontWeight: FontWeight.w500);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title: Text('Dettagli'),
        ),
        body: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            CardDoppia(
                widget1: Text('ID prenotazione: ', style: _stileMain),
                widget2: Text(widget.prenotazione.id, style: _stileMain)),
            CardDoppia(
                widget1: Text('Stanza: ', style: _stileMain),
                widget2: Text(widget.prenotazione.postazione.room,
                    style: _stileMain)),
            CardDoppia(
                widget1: Text('Nome postazione: ', style: _stileMain),
                widget2: Text(widget.prenotazione.postazione.name,
                    style: _stileMain)),
            CardDoppia(
                widget1: Text('Inizio prenotazione: ', style: _stileMain),
                widget2: Text(
                    DateFormat('yyyy-MM-dd HH:mm:ss').format(
                        widget.prenotazione.inizio.add(Duration(hours: 2))),
                    style: _stileMain)),
            CardDoppia(
                widget1: Text('Fine prenotazione: ', style: _stileMain),
                widget2: Text(
                    DateFormat('yyyy-MM-dd HH:mm:ss').format(
                        widget.prenotazione.fine.add(Duration(hours: 2))),
                    style: _stileMain)),
            widget.eliminabile
                ? ElevatedButton(
                    onPressed: _showDeleteDialog,
                    child: Text('Elimina prenotazione'))
                : Container(),
            widget.prenotazione.cancellazione != null
                ? CardDoppia(
                    widget1: Text('Data cancellazione: ', style: _stileMain),
                    widget2: Text(
                        DateFormat('yyyy-MM-dd HH:mm:ss').format(widget
                            .prenotazione.cancellazione
                            .add(Duration(hours: 2))),
                        style: _stileMain))
                : Container()
          ],
        ));
  }

  void _showDeleteDialog() async {
    bool ris = await showDialog(
        context: context,
        builder: (BuildContext context) => DialogDeletePrenotazione(
              prenotazione: widget.prenotazione,
            ));
    if (ris == null) {
      return;
    }
    if (ris) {
      await showDialog(
          context: context,
          builder: (BuildContext context) =>
              DialogRisultato(risultato: 'Eliminazione avvenuta!'));
      // Se ho eliminato poppo il dettaglio di questa prenotazione e ritorno true, indicando che voglio un refresh della
      // lista delle prenotazioni
      Navigator.of(context).pop(true);
    } else {
      await showDialog(
          context: context,
          builder: (BuildContext context) =>
              DialogRisultato(risultato: 'Errore nell\'eliminazione'));
    }
  }
}
