import 'package:flutter/material.dart';

class ErroreNfcNonPresente extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
            'NFC non presente sul dispositivo.\nControllare i requisiti dell\'applicazione prima di installarla.'),
      ),
    );
  }
}
