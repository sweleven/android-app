import 'package:block_covid/extras/WidgetTool.dart';
import 'package:block_covid/pages/HomeStanzaDaPulire.dart';
import 'package:block_covid/pages/HomeStanzePulite.dart';
import 'package:flutter/material.dart';

class HomeCleaner extends StatefulWidget {
  @override
  _HomeCleanerState createState() => _HomeCleanerState();
}

class _HomeCleanerState extends State<HomeCleaner> {
  String _title = "Stanze usate";
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        key: Key('Tabcontroller'),
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            leading: WidgetTool(),
            //backgroundColor: Colors.green,
            title: Text(_title),
            bottom: TabBar(
              onTap: _tabBarTapped,
              tabs: [
                Tab(
                  key: Key('StanzeUsateButton'),
                  icon: Icon(
                    Icons.meeting_room,
                  ),
                  text: 'Stanze usate',
                ),
                Tab(
                    key: Key('StanzeIgienizzateButton'),
                    icon: Icon(Icons.list),
                    text: 'Stanze igienizzate oggi'),
              ],
            ),
          ),
          body: TabBarView(key: Key('addPages'), children: [
            HomeStanzaDaPulire(),
            HomeStanzePulite(),
          ]),
        ));
  }

  void _tabBarTapped(int index) {
    setState(() {
      switch (index) {
        case 0:
          _title = 'Stanze usate';
          break;
        case 1:
          _title = 'Stanze igienizzate';
          break;
      }
    });
  }
}
