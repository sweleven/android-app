import 'package:block_covid/api/ApiServiceEmployee.dart';
import 'package:block_covid/extras/WidgetTool.dart';
import 'package:block_covid/extras/dialogs/DialogRisultato.dart';
import 'package:block_covid/model/GpsManager.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:block_covid/extras/dialogs/DialogNfcPostRead.dart';
import 'package:block_covid/extras/dialogs/DialogNfcRead.dart';
import 'package:block_covid/pages/CreaPrenotazione.dart';
import 'package:block_covid/pages/HomeListaPrenotazioni.dart';
import 'package:flutter/material.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';
import 'package:http/http.dart' as http;

class HomeEmployee extends StatefulWidget {
  final ApiServiceEmployee _apiServiceEmployee = ApiServiceEmployee();
  @override
  _HomeEmployeeState createState() => _HomeEmployeeState();
}

class _HomeEmployeeState extends State<HomeEmployee> {
  String _title = 'Prenotazioni attive';
  GpsManager _gpsManager;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        key: Key("Home"),
        initialIndex: 1,
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            leading: WidgetTool(),
            title: Text(_title),
            bottom: TabBar(
              onTap: _tabBarTapped,
              tabs: [
                Tab(icon: Icon(Icons.calendar_today_rounded)),
                Tab(icon: Icon(Icons.list, key: Key("prenotazioni-attive"))),
                Tab(
                    icon: Icon(Icons.line_style,
                        key: Key("storico-prenotazioni"))),
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.nfc),
            onPressed: _showNfcDialog,
          ),
          body: TabBarView(children: [
            CreaPrenotazione(),
            HomeListaPrenotazioni(mostraSoloAttive: true),
            HomeListaPrenotazioni(mostraSoloAttive: false),
          ]),
        ));
  }

  void _tabBarTapped(int index) {
    setState(() {
      switch (index) {
        case 0:
          _title = 'Crea prenotazione';
          break;
        case 1:
          _title = 'Prenotazioni attive';
          break;
        case 2:
          _title = 'Prenotazioni effettuate';
          break;
      }
    });
  }

  @override
  void dispose() {
    FlutterNfcReader.stop();
    _gpsManager.stop();
    super.dispose();
  }

  Future _stampaPremuto(String payload) {
    print('Premuto sulla notifica con payload: $payload');
    return null;
  }

  @override
  void initState() {
    super.initState();
    _gpsManager = GpsManager(callBack: _stampaPremuto);
  }

  /// Invocata dal pulsante leggi tag NFC, mostra il dialog per leggere
  /// il tag NFC, e se la scannerizzazione va a buon fine elabora il tag letto
  /// e mostra il dialog corispondente allo stato del tag letto.
  void _showNfcDialog() async {
    NFCAvailability stato = await FlutterNfcReader.checkNFCAvailability();
    if (stato == NFCAvailability.disabled) {
      await showDialog(
          context: context,
          builder: (context) =>
              DialogRisultato(risultato: 'Si è pregati di attivare l\'NFC.'));
      return;
    }
    String tag = await showDialog(
        context: context, builder: (context) => DialogNfcRead());
    FlutterNfcReader.stop();
    if (tag == null) {
      // l'utente ha fatto exit o ha tappato fuori dal dialog
      return;
    }
    Postazione p = await widget._apiServiceEmployee
        .getPostazioneFromRfid(http.Client(), tag);
    await showDialog(
        context: context,
        builder: (context) => DialogNfcPostRead(
              postazione: p,
              gpsManager: _gpsManager,
            ));
  }
}
