import 'package:block_covid/api/ApiServiceEmployee.dart';
import 'package:block_covid/extras/ListaVuotaRicarica.dart';
import 'package:block_covid/extras/cards/CardPrenotazioni.dart';
import 'package:block_covid/model/Prenotazione.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class HomeListaPrenotazioni extends StatefulWidget {
  HomeListaPrenotazioni({@required this.mostraSoloAttive});
  final ApiServiceEmployee _apiServiceEmployee = ApiServiceEmployee();
  final bool mostraSoloAttive;

  @override
  _HomeListaPrenotazioniState createState() => _HomeListaPrenotazioniState();
}

class _HomeListaPrenotazioniState extends State<HomeListaPrenotazioni>
    with AutomaticKeepAliveClientMixin<HomeListaPrenotazioni> {
  List<Prenotazione> _listaPrenotazioni;
  Future<List<Prenotazione>> _lista;
  bool _ricarica;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilder(
        // Se ricarica è true provengo dal pull to refresh, altrimenti devo aspettare il primo
        // caricamento di _lista.
        key: Key("StoricoPrenotazioni"),
        future: _ricarica
            ? (!widget.mostraSoloAttive
                ? widget._apiServiceEmployee.getPrenotazioni(
                    http.Client(),
                    DateTime(2020),
                    DateTime.now().add(Duration(days: 35)),
                    !widget.mostraSoloAttive)
                : widget._apiServiceEmployee.getPrenotazioni(
                    http.Client(),
                    DateTime.now(),
                    DateTime.now().add(Duration(days: 36)),
                    !widget.mostraSoloAttive))
            : _lista,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              _listaPrenotazioni = snapshot.data;
              // Azsero ricarica, perchè ho i dati.
              _ricarica = false;
              // Se non ho nessuna prenotazione con queste caratteristiche
              if (_listaPrenotazioni.length == 0) {
                return ListaVuotaRicarica(
                    testo: 'Nessuna prenotazione',
                    pressed: () {
                      setState(() {
                        _ricarica = true;
                      });
                    });
              }
              _listaPrenotazioni.sort((a, b) => b.inizio.compareTo(a.inizio));
              return RefreshIndicator(
                  onRefresh: () async {
                    setState(() {
                      _ricarica = true;
                      // NON chiedo ricarica perché la faccio già implicitamente qua,
                      // così il FUtureBuilder vede il Future già
                      //_listaPrenotazioni = await ApiService.getEmployees();
                    });
                    return;
                  },
                  child: _creaListView());
            } else {
              // Ha chiuso la connessione ma l'api ha ritornato un errore
              return Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(15),
                    child: Text('Errore nel caricamento, riprovare grazie',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.red, fontSize: 30)),
                  ),
                  ElevatedButton.icon(
                      onPressed: () {
                        setState(() {
                          // Attivo ricarica perché l'ho chiesta.
                          _ricarica = true;
                        });
                      },
                      icon: Icon(Icons.refresh),
                      label: Text('Ricarica'))
                ],
              );
            }
          }

          // Devo wrapparlo in un Column sennò il progressindicator non renderizza bene
          return Column(
            children: [
              SizedBox(height: 20),
              CircularProgressIndicator(),
            ],
          );
        });
  }

  /// Crea la ListView sulla base della Map list locale
  ListView _creaListView() {
    return ListView.builder(
      itemBuilder: (_, index) => index != _listaPrenotazioni.length
          ? CardPrenotazioni(
              prenotazione: _listaPrenotazioni[index],
              eliminabile: widget.mostraSoloAttive,
            )
          : Center(
              child: ElevatedButton.icon(
                  onPressed: () {
                    setState(() {
                      // Attivo ricarica perché l'ho chiesta.
                      _ricarica = true;
                    });
                  },
                  icon: Icon(Icons.refresh),
                  label: Text('Ricarica')),
            ),
      itemCount: _listaPrenotazioni.length + 1,
      // Rende sempre scrollabile la lista, anche con un solo elemento
      physics: const AlwaysScrollableScrollPhysics(),
    );
  }

  @override
  void initState() {
    super.initState();
    // Fa partire la richiesta asincrona,
    // poi il FutureBuilder la aspetterà.
    if (widget.mostraSoloAttive) {
      _lista = widget._apiServiceEmployee.getPrenotazioni(
          http.Client(),
          DateTime.now(),
          DateTime.now().add(Duration(days: 36)),
          !widget.mostraSoloAttive);
    } else {
      _lista = widget._apiServiceEmployee.getPrenotazioni(
          http.Client(),
          DateTime(2020),
          DateTime.now().add(Duration(days: 35)),
          widget.mostraSoloAttive);
    }
    _ricarica = false;
  }

  @override
  bool get wantKeepAlive => true;
}
