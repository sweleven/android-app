import 'package:block_covid/api/ApiService.dart';
import 'package:block_covid/api/ApiServiceCleaner.dart';
import 'package:block_covid/extras/ListaVuotaRicarica.dart';
import 'package:block_covid/extras/dialogs/DialogRisultato.dart';
import 'package:block_covid/model/Stanza.dart';
import 'package:block_covid/extras/dialogs/DialogNfcReadIgienizzatore.dart';
import 'package:block_covid/extras/dialogs/DialogRisultatoIgienizzatore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';
import 'package:http/http.dart' as http;

///Crea la lista delle stanze da pulire prelevando i dati dal server.
///
///Ogni sezione della lista avrà L'ID e nome e avrà anche il bottone per fare la scansione
class HomeStanzaDaPulire extends StatefulWidget {
  final ApiServiceCleaner _apiServiceCleaner = ApiServiceCleaner();
  @override
  _HomeStanzaDaPulireState createState() => _HomeStanzaDaPulireState();
}

class _HomeStanzaDaPulireState extends State<HomeStanzaDaPulire> {
  @override
  Widget build(BuildContext context) {
    print('ricreato');
    return Container(
      child: FutureBuilder(
        future: widget._apiServiceCleaner.getStanzeSporche(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Stanza> _listaStanzeSporche = snapshot.data;
            if (_listaStanzeSporche.isEmpty) {
              return ListaVuotaRicarica(
                testo: 'Nessuna stanza da pulire',
                pressed: () {
                  setState(() {});
                },
              );
            }
            return RefreshIndicator(
              key: Key('RefreshIndicator'),
              onRefresh: _refreshData,
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  height: 20,
                  thickness: 2.0,
                ),
                itemBuilder: (_, index) {
                  return ListTile(
                    title: Text(
                      _listaStanzeSporche[index].name,
                    ),
                    trailing: IconButton(
                      icon: Icon(Icons.nfc, key: Key('NfcButton'), size: 40),
                      onPressed: () {
                        createAlertDialog(context, _listaStanzeSporche[index]);
                      },
                    ),
                  );
                },
                itemCount: _listaStanzeSporche.length,
              ),
            );
          } else
            return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  ///Funzione che ricarica la pagina e aggiorna la lista
  Future _refreshData() async {
    setState(() {});
  }

  ///Crea dialogo che gestisce la letturra del tag nfc di ogni stanza
  ///
  ///Alla lettura della stanza ritorna l'ID della stanza e crea diversi dialog:
  ///
  ///Se il [tag] è null l'igienizzazione è annullata;
  ///
  ///Se il [tag] è diverso dall'ID della stanza corrente allora è della stanza sbagliata;
  ///
  ///Se il [tag] è corretto ritorna l'effettiva igienizzazione della stanza
  void createAlertDialog(BuildContext context, Stanza stanza) async {
    NFCAvailability stato = await FlutterNfcReader.checkNFCAvailability();
    if (stato == NFCAvailability.disabled) {
      await showDialog(
          context: context,
          builder: (context) =>
              DialogRisultato(risultato: 'Si è pregati di attivare l\'NFC.'));
      return;
    }
    String tag = await showDialog(
        context: context,
        builder: (contex) {
          return DialogNfcReadIgienizzatore(stanza);
        });
    FlutterNfcReader.stop();
    if (tag == null) {
      return showDialog(
          context: context,
          builder: (context) => DialogRisultatoIgienizzatore(
                risultato: 'Igienizzazione annullata',
                verificato: false,
              ));
    }
    tag = tag.substring(1);
    if (tag != stanza.rfid) {
      return showDialog(
          context: context,
          builder: (context) => DialogRisultatoIgienizzatore(
                risultato: 'RFID stanza errato',
                verificato: false,
              ));
    } else {
      bool ris = await ApiService().pulisci(http.Client(), tag);
      ris
          ? await showDialog(
              context: context,
              builder: (context) => DialogRisultatoIgienizzatore(
                risultato: 'Igienizzazione avvenuta con successo',
                verificato: true,
              ),
            )
          : await showDialog(
              context: context,
              builder: (context) => DialogRisultatoIgienizzatore(
                risultato: 'Igienizzazione non avvenuta corettamente',
                verificato: false,
              ),
            );
      setState(() {});
    }
  }
}
