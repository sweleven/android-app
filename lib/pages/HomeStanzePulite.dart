import 'package:block_covid/api/ApiServiceCleaner.dart';
import 'package:block_covid/extras/ListaVuotaRicarica.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class HomeStanzePulite extends StatefulWidget {
  final ApiServiceCleaner _apiServiceCleaner = ApiServiceCleaner();
  @override
  _HomeStanzePuliteState createState() => _HomeStanzePuliteState();
}

class _HomeStanzePuliteState extends State<HomeStanzePulite> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: widget._apiServiceCleaner.getStanzePuliteOggi(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            Map<String, List<dynamic>> mappaStanzePulite = snapshot.data;
            if (mappaStanzePulite['listaStanze'].isEmpty) {
              return ListaVuotaRicarica(
                testo: 'Nessuna stanza recentemente pulita',
                pressed: () {
                  setState(() {});
                },
              );
            }
            return RefreshIndicator(
              onRefresh: _refreshData,
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  height: 2,
                  thickness: 2.0,
                ),
                itemBuilder: (_, index) {
                  DateTime orario = mappaStanzePulite['listaOrari'][index];
                  return ListTile(
                    title: Text(
                      mappaStanzePulite['listaStanze'][index].name,
                    ),
                    subtitle: Text('Orario di pulizia: ' +
                        DateFormat('yyyy-MM-dd HH:mm:ss')
                            .format(orario.add(Duration(hours: 2)))),
                  );
                },
                itemCount: mappaStanzePulite['listaStanze'].length,
              ),
            );
          } else
            return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Future _refreshData() async {
    setState(() {});
  }
}
