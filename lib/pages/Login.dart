// ignore_for_file: unused_local_variable
import 'package:block_covid/api/ApiKeycloak.dart';
// ignore: unused_import
import 'package:block_covid/pages/ErroreNfcNonPresente.dart';
import 'package:block_covid/pages/HomeEmployee.dart';
import 'package:block_covid/pages/HomeCleaner.dart';
import 'package:flutter/material.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';

class Login extends StatefulWidget {
  final ApiKeycloak _apiKeycloak = ApiKeycloak();
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: FlutterNfcReader.checkNFCAvailability(), builder: _builderNfc);
  }

  /// Builder principale del FutureBuilder.
  Widget _builder(
      BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
    if (snapshot.connectionState == ConnectionState.done) {
      if (snapshot.hasData) {
        String ruolo = snapshot.data['realm_access']['roles'][0];
        if (ruolo == 'cleaner') {
          print('Loggato come cleaner');
          return HomeCleaner();
        }
        if (ruolo == 'employee') {
          print('Loggato come employee');
          return HomeEmployee();
        }
        throw 'Errore nel login: ruolo non riconosciuto';
      }
    }
    return Scaffold(
        body: Container(
            color: Colors.white,
            child: Center(child: Text('Connettiti alla VPN, per favore'))));
  }

  Widget _builderNfc(
      BuildContext context, AsyncSnapshot<NFCAvailability> snapshot) {
    if (snapshot.hasData) {
      print('Stato NFC: ' + snapshot.data.toString());
      // if (true) {
      // : mettere al posto che true: snapshot.data != NFCAvailability.not_supported
      return FutureBuilder(
          key: Key('Login'),
          future: widget._apiKeycloak.logIn(),
          builder: _builder);
      // ignore: dead_code
      //} else {
      //  return ErroreNfcNonPresente();
      //}
    }
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Future<Map<String, dynamic>> fakeFoo() async {
    Map<String, dynamic> m1 = {
      'roles': ['employee']
    };
    return {'realm_access': m1};
  }
}
