import 'package:block_covid/api/ApiServiceEmployee.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:block_covid/model/Prenotazione.dart';
import 'package:block_covid/extras/dialogs/DialogRisultato.dart';
import 'package:block_covid/extras/dialogs/DialogSelezionaOraFine.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

/// Si occupa di mostrare la schermata di preview della conferma della prenotazione
class PreviewPrenotazione extends StatefulWidget {
  PreviewPrenotazione(
      {@required this.postazione,
      @required this.oraFineModificabile,
      this.fine,
      this.inizio});

  /// Postazione di cui si vuole effettuare la prenotazione.
  final Postazione postazione;

  /// Inizio della prenotazione.
  final DateTime inizio;

  /// Fine della prenotazione.
  final DateTime fine;

  /// Indica se la fine della prenotazione è modificabile. Serve quando si arriva dalla
  /// classe [DialogNfcPostRead]
  final bool oraFineModificabile;

  final ApiServiceEmployee _apiServiceEmployee = ApiServiceEmployee();

  @override
  _PreviewPrenotazioneState createState() => _PreviewPrenotazioneState();
}

class _PreviewPrenotazioneState extends State<PreviewPrenotazione> {
  DateTime _fineEditabile;
  TextEditingController _controllerFine;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Conferma prenotazione'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Form(
            child: Column(
          children: [
            TextFormField(
                initialValue: widget.inizio.toString().substring(0, 10),
                enabled: false,
                decoration: InputDecoration(
                    labelText: 'Data', icon: Icon(Icons.date_range))),
            TextFormField(
                initialValue: widget.inizio.toString().substring(10, 19),
                enabled: false,
                decoration: InputDecoration(
                  labelText: 'Ora di inizio',
                  icon: Icon(Icons.timer),
                )),
            TextFormField(
                controller: _controllerFine,
                enabled: widget.oraFineModificabile,
                readOnly: true,
                onTap: widget.oraFineModificabile
                    ? () async {
                        var fine = await showDialog(
                            context: context,
                            builder: (context) =>
                                DialogSelezionaOraFine(widget.inizio));
                        if (fine != null) {
                          _fineEditabile = fine;
                        } else {
                          return;
                        }
                        setState(() {
                          _controllerFine.text =
                              _fineEditabile.toString().substring(11, 19);
                        });
                      }
                    : null,
                decoration: InputDecoration(
                  labelText: 'Ora di fine',
                  icon: Icon(Icons.timer_off),
                )),
            TextFormField(
                initialValue: widget.postazione.name,
                enabled: false,
                decoration: InputDecoration(
                    labelText: 'Postazione', icon: Icon(Icons.desktop_mac))),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: ElevatedButton(
                  onPressed: _prenotaPostazione, child: Text('Prenota')),
            ),
          ],
        )),
      ),
    );
  }

  /* Future<void> _selezionaOraFine(BuildContext context) async {
    TimeOfDay oraSelezionata = await showTimePicker(
        helpText: "Seleziona l'orario",
        cancelText: 'Annulla',
        confirmText: 'Conferma',
        context: context,
        initialTime: TimeOfDay.fromDateTime(_fineEditabile),
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
              data:
                  MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
              child: child);
        });
    if (oraSelezionata != null) {
      // Controllo che l'ora selezionata sia dopo l'ora di inizio
      DateTime selezionato = DateTime(widget.inizio.year, widget.inizio.month,
          widget.inizio.day, oraSelezionata.hour, oraSelezionata.minute);
      DateTime now = DateTime.now();
      DateTime oggiAlle20 = DateTime(now.year, now.month, now.day, 20, 0, 0);
      if (selezionato.isAfter(widget.inizio) &&
          selezionato.isBefore(oggiAlle20))
        setState(() {
          _fineEditabile = selezionato;
          _controllerFine.text = _fineEditabile.toString().substring(11, 19);
        });
    }
  }*/

  void _prenotaPostazione() async {
    bool risultato = await widget._apiServiceEmployee.createReservation(
        http.Client(),
        Prenotazione(
            postazione: widget.postazione,
            inizio: widget.inizio,
            fine: _fineEditabile));
    if (risultato == null || risultato == false) {
      await showDialog(
          context: context,
          builder: (context) => DialogRisultato(
              risultato: 'Errore nella prenotazione, riprova.'));
    } else {
      await showDialog(
          context: context,
          builder: (context) => DialogRisultato(
              risultato: 'Prenotazione effettuata con successo!'));
      Navigator.of(context).popUntil((route) => route.isFirst);
    }
  }

  @override
  void initState() {
    super.initState();
    _fineEditabile = widget.fine;
    _controllerFine = TextEditingController(
        text: _fineEditabile.toString().substring(11, 19));
  }

  @override
  void dispose() {
    _controllerFine.dispose();
    super.dispose();
  }
}
