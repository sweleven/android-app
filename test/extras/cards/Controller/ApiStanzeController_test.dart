/* // ignore_for_file: unused_import, unused_local_variable
import 'package:block_covid/model/StanzaCleaner.dart';
import 'package:flutter_test/flutter_test.dart';
//import 'package:http/testing.dart';
//import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';

class MockitoExample extends Mock implements http.Client {}

void main() {
  group('getSingolaStanza', () {
    test('Ritorna la singola stanza usata se la chiamata va a buon fine',
        () async {
      http.Client mockitoExample = MockitoExample();
      String id = '1';

      when(mockitoExample.get(Uri.parse(
              'https://my-json-server.typicode.com/wahad88/Mockjson' + id)))
          .thenAnswer((_) async => http.Response(
              '{"id": "1", "name": "Stanza 1", "position": "Piano Primo"}',
              200));

      expect(await ApiServiceStanzeaCleaner().getSingolaStanza(id),
          isA<StanzaAta>());
    });

    test('Eccezione se la chiamata a get per singola stanza finisce in errore',
        () {
      final mockitoExample = MockitoExample();
      String id = '-1';

      when(mockitoExample.get(Uri.parse(
              'https://my-json-server.typicode.com/wahad88/Mockjson/stanza_usata' +
                  id)))
          .thenAnswer((_) async => http.Response('Not Found', 404));

      expect(ApiServiceStanzeaCleaner().getSingolaStanza(id), throwsException);
    });
  });
  group('aggiungi stanza', () {
    test('Aggiungo una stanza alla lista delle stanze igienizzate', () async {
      final mockitoExample = MockitoExample();
      StanzaAta prova = StanzaAta();

      when(mockitoExample.post(
          Uri.parse(
              'https://my-json-server.typicode.com/wahad88/Mockjson/stanza_igienizzata/'),
          body: {
            "id": prova.id,
            "name": prova.name,
            "position": prova.position,
          })).thenAnswer((_) async => http.Response('Somedata', 201));

      expect(await ApiServiceStanzeaCleaner().aggiungiStanza(prova),
          isA<StanzaAta>());
    });
  });

  group('delete', () {
    test('delete una stanza dalla lista delle stanze', () async {
      final mockitoExample = MockitoExample();
      String id = '1';

      when(mockitoExample.delete(Uri.parse(
              'https://my-json-server.typicode.com/wahad88/Mockjson/stanza_usata' +
                  id)))
          .thenAnswer((_) async => http.Response('true', 200));

      expect(await ApiServiceStanzeaCleaner().deleteStanza(id), true);
    });

    test('delete non va a buon fine', () {
      final mockitoExample = MockitoExample();
      String id = '-1';

      when(mockitoExample.delete(Uri.parse(
              'https://my-json-server.typicode.com/wahad88/Mockjson/stanza_usata' +
                  id)))
          .thenAnswer((_) async => http.Response('false', 404));

      expect(ApiServiceStanzeaCleaner().deleteStanza(id), throwsException);
    });
  });
}
 */
