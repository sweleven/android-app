// ignore_for_file: unused_local_variable
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:block_covid/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('end-to-end test', () {
    testWidgets('Logged in', (WidgetTester tester) async {
      app.main();
      //pattern da seguire
      //pumpsettle
      //finders
      //taps and others staff?
      //expect
      //repeat
      final Finder log = find.byKey(Key("Login"));
      await tester.pumpAndSettle(Duration(seconds: 3));
      expect(log, findsOneWidget);
      //POST ha creato la finestra di VPN
      await tester.pumpAndSettle(Duration(seconds: 15));
      final Finder home = find.byKey(
          Key("Home")); //da mettere quello la key della classe del cleaner
      expect(home, findsOneWidget);

      //Home creata per il cleaner
      //expect cleaner
    });
    //expect(vpn, findsOneWidget);
  });
}
