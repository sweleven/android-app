// ignore_for_file: unused_local_variable
// ignore_for_file: unused_import
import 'package:block_covid/extras/dialogs/DialogConfermaPrenotazione.dart';
import 'package:block_covid/extras/dialogs/DialogSelezionaOraFine.dart';
import 'package:block_covid/pages/HomeListaPrenotazioni.dart';
import 'package:block_covid/extras/cards/CardPrenotazioni.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:block_covid/main.dart' as app;
import 'package:block_covid/extras/dialogs/DialogErrore.dart';
import 'package:block_covid/extras/dialogs/DialogRispostaValida.dart';
import 'package:block_covid/extras/dialogs/ListViewDialogGenerale.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('end-to-end test', () {
    testWidgets('Employee is Logged', (WidgetTester tester) async {
      app.main();
      //find if the Webview Responde
      //final Finder home = find.byKey(Key('CreaPrenotazione'));

      await tester.pumpAndSettle(Duration(seconds: 5));
      final Finder log = find.byKey(Key("Login"));
      expect(log, findsOneWidget);
      await tester.pumpAndSettle(Duration(seconds: 2));
      await tester.pumpAndSettle(Duration(seconds: 1));
      //POST ha creato la finestra di VPN (se serviva)
      // NON SI PUÒ FARE PERCHÈ NON È NATIVO passare parametri che simulino il login web
      //final Finder home = find.byKey(Key("Home"));
      final Finder home = find.byType(Tab);
      await tester.tap(home.first);
      await tester.pumpAndSettle(Duration(seconds: 1));
      final Finder cprenotazione = find.byKey(Key("CreaPrenotazione"));
      expect(cprenotazione, findsOneWidget);
      await tester.pumpAndSettle(Duration(seconds: 1));

      //form campo data esiste
      final Finder data = find.byKey(Key("data"));
      expect(data, findsOneWidget);

      ///errore ora inizio data non inserita
      final Finder orainizio = find.byKey(Key("orainizio"));
      expect(orainizio, findsOneWidget);
      await tester.tap(orainizio);
      await tester.pumpAndSettle(Duration(seconds: 2));
      final Finder datanotsetted = find.byType(DialogErrore);
      expect(datanotsetted, findsOneWidget);
      Finder iconexit = find.byIcon(Icons.exit_to_app);
      await tester.tap(iconexit);
      await tester.pumpAndSettle();
      Finder declosed = find.byType(DialogErrore);
      expect(declosed, findsNothing);

      ///errore durata non inserita
      final Finder durata = find.byKey(Key("durata"));
      expect(durata, findsOneWidget);
      await tester.tap(durata);
      await tester.pumpAndSettle(Duration(seconds: 1));
      final Finder duratanotsetted = find.byType(DialogErrore);
      expect(duratanotsetted, findsOneWidget);
      iconexit = find.byIcon(Icons.exit_to_app);
      await tester.tap(iconexit);
      await tester.pumpAndSettle(Duration(seconds: 1));
      declosed = find.byType(DialogErrore);
      expect(declosed, findsNothing);

      ///errore dati precedenti non inseriti
      final Finder selstanza = find.byKey(Key("selstanza"));
      expect(selstanza, findsOneWidget);
      await tester.tap(selstanza);
      await tester.pumpAndSettle(Duration(seconds: 1));
      Finder notsetted = find.byType(DialogErrore);
      expect(notsetted, findsOneWidget);
      iconexit = find.byIcon(Icons.exit_to_app);
      await tester.tap(iconexit);
      await tester.pumpAndSettle(Duration(seconds: 1));
      declosed = find.byType(DialogErrore);
      expect(declosed, findsNothing);

      ///errore postazione stanza non sel
      final Finder selpostazione = find.byKey(Key("selpostazione"));
      expect(selpostazione, findsOneWidget);
      await tester.tap(selpostazione);
      await tester.pumpAndSettle(Duration(seconds: 1));
      notsetted = find.byType(DialogErrore);
      expect(notsetted, findsOneWidget);
      iconexit = find.byIcon(Icons.exit_to_app);
      await tester.tap(iconexit);
      await tester.pumpAndSettle(Duration(seconds: 1));
      declosed = find.byType(DialogErrore);
      expect(declosed, findsNothing);

      ///errore prenota senza inserire tutti i campi dati
      final Finder bottoneprenota = find.byKey(Key("bottoneprenota"));
      expect(bottoneprenota, findsOneWidget);
      await tester.tap(bottoneprenota);
      await tester.pumpAndSettle(Duration(seconds: 1));
      notsetted = find.byType(DialogErrore);
      expect(notsetted, findsOneWidget);
      iconexit = find.byIcon(Icons.exit_to_app);
      await tester.tap(iconexit);
      await tester.pumpAndSettle(Duration(seconds: 1));
      declosed = find.byType(DialogErrore);
      expect(declosed, findsNothing);
      //errore sotto la form campi dati VISIBILE(NON SERVE LO OFFRE IL FRAMEWORK)

      //seleziona data
      await tester.tap(data);
      await tester.pumpAndSettle(Duration(seconds: 1));
      await tester.tap(find.text('OK'));
      await tester.pumpAndSettle(Duration(seconds: 1));
      //seleziona orario

      //ora sbagliata
      /*await tester.tap(orainizio);
      var center = tester
          .getCenter(find.byKey(const ValueKey<String>('time-picker-dial')));

      await tester.tapAt(Offset(center.dx - 50, center.dy));
      Finder badsetted = find.byType(DialogErrore);
      expect(badsetted, findsOneWidget);
      iconexit = find.byIcon(Icons.exit_to_app);
      await tester.tap(iconexit);
      await tester.pumpAndSettle();
      await tester.pumpAndSettle(Duration(seconds: 1));*/

      //ora giusta (giornata di oggi)
      await tester.tap(orainizio);
      await tester.pumpAndSettle(Duration(seconds: 1));
      await tester.tap(find.text('OK'));
      await tester.pumpAndSettle(Duration(seconds: 1));
      //seleziona durata (1 ore)
      await tester.tap(durata);
      await tester.pumpAndSettle(Duration(seconds: 3));
      final Finder uidurata = find.byKey(Key("dialog-durata"));
      expect(uidurata, findsOneWidget);
      await tester.pumpAndSettle(Duration(seconds: 1));
      // final Finder bdurata = find.byKey(Key("durata"));

      final Finder quanto = find.byKey(Key("durata"));
      //find.text('resto della giornata');
      await tester.tap(quanto.first);
      await tester.pumpAndSettle(Duration(seconds: 3));
      //tester.takeException();
      //seleziona stanza (la prima che trova)
      await tester.tap(find.byKey(Key("selstanza")));
      await tester.pumpAndSettle(Duration(seconds: 6));
      final Finder stanzalib = find.byKey(Key("LVDG"));
      await tester.pumpAndSettle(Duration(seconds: 6));
      expect(stanzalib, findsOneWidget);
      final Finder stanzabtn = find.byKey(Key("LVDG-button"));
      await tester.tap(stanzabtn.first, warnIfMissed: false);
      await tester.pumpAndSettle(Duration(seconds: 6));
      //sel postazione (la prima della lista)
      await tester.tap(selpostazione);
      await tester.pumpAndSettle(Duration(seconds: 5));
      final Finder postazionelib = find.byKey(Key("LVDG"));
      await tester.tap(postazionelib.first);
      await tester.pumpAndSettle(Duration(seconds: 4));
      //bottoneprenotazione (tenta la prenotazione con successo?!)
      await tester.tap(bottoneprenota, warnIfMissed: false);
      await tester.pumpAndSettle(Duration(seconds: 1));
      final Finder riepilogo = find.byType(DialogConfermaPrenotazione);
      expect(riepilogo, findsOneWidget);
      final Finder conferma = find.byKey(Key("btnprenota"));
      await tester.tap(conferma, warnIfMissed: false);
      await tester.pumpAndSettle(Duration(seconds: 3));
      final Finder prenotato = find.byType(DialogRispostaValida);
      //risposta positiva prenotazione effettuata
      expect(prenotato, findsOneWidget);
      await tester.pumpAndSettle(Duration(seconds: 1));
      //CHIUDI FINESTRA DI SUCCESSO
      final Finder chiudisuc = find.byKey(Key("success-btn"));
      await tester.tap(chiudisuc);
      await tester.pumpAndSettle(Duration(seconds: 1));

      //pernotazione con stessi dati risposta negativa
      await tester.tap(bottoneprenota, warnIfMissed: false);
      await tester.pumpAndSettle(Duration(seconds: 1));
      final Finder riepilogoer = find.byType(DialogConfermaPrenotazione);
      expect(riepilogoer, findsOneWidget);
      final Finder confermaer = find.byKey(Key("btnprenota"));
      await tester.tap(confermaer, warnIfMissed: false);
      await tester.pumpAndSettle(Duration(seconds: 3));
      await tester.pumpAndSettle();
      final Finder prenotatoer = find.byType(DialogErrore);
      expect(prenotatoer, findsOneWidget);
      await tester.pumpAndSettle(Duration(seconds: 1));
      Finder iconexiter = find.byIcon(Icons.exit_to_app);
      await tester.tap(iconexiter);
      await tester.pumpAndSettle();
      //risposta negativa FINE

      //switch tab a quelle prenotate INCOMBENTI
      final Finder listatab = find.byKey(Key("prenotazioni-attive"));
      await tester.tap(listatab);
      await tester.pumpAndSettle(Duration(seconds: 1));
      final Finder listapren = find.byType(HomeListaPrenotazioni).first;
      final Finder prenicombente = find.descendant(
          of: listapren, matching: find.byType(CardPrenotazioni).first);
      await tester.tap(prenicombente.first);
      await tester.pumpAndSettle(Duration(seconds: 1));
      await tester.pumpAndSettle(Duration(seconds: 4));
    });
  });
}
