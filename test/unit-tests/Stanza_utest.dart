// Import the test package and Counter class
/*import 'dart:io';

import 'package:block_covid/model/Stanza.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:block_covid/model/Postazione.dart';
import 'dart:convert';

void main() {
  group('StanzaModel', () {
    //questo dovrebbe testare se i tag required bloccano (non lo fanno)
    test('StanzaModel should be initialized(NULL)', () {
      expect(Stanza().name, null);
    });
    //TODO test setters getters message error nullsafety?
    test('StanzaModel should be initialized', () {
      final counter = Stanza(
          id: 'sdadsa',
          name: 'name',
          v: 0,
          listapostazioni: <Postazione>[
            Postazione(
                id: 'pos1', name: 'ciao', room: 'sdadsa', rfid: 'codiceRFID')
          ],
          totalepostazioni: 1);
      //nome uguale
      expect(
          counter.name,
          Stanza(
                  id: 'sdadsa',
                  name: 'name',
                  v: 0,
                  listapostazioni: <Postazione>[
                    Postazione(
                        id: 'pos1',
                        name: 'ciao',
                        room: 'sdadsa',
                        rfid: 'codiceRFID')
                  ],
                  totalepostazioni: 1)
              .name);
      //idstanza identico per le postazioni all'interno
      expect(
          counter.id,
          Stanza(
                  id: 'sdadsa',
                  name: 'name',
                  v: 0,
                  listapostazioni: <Postazione>[
                    Postazione(
                        id: 'pos1',
                        name: 'ciao',
                        room: 'sdadsa',
                        rfid: 'codiceRFID')
                  ],
                  totalepostazioni: 1)
              .listapostazioni
              .first
              .room);
    });
    test('Stanza() from a JSON', () {
      String jstring =
          '{"_id": "6069caf5fad3c40029f651fe","name": "2","__v": 0,"workspaces": [{"_id": "6069cb15fad3c40029f651ff","room": "6069caf5fad3c40029f651fe","name": "1","rfid": "rfidCODE","__v": 0}],"workspaces_total": 1}';
      Map<String, dynamic> json = jsonDecode(jstring);
      final counter = Stanza.fromJson(json);
      expect(counter.listapostazioni.first.name,
          Stanza.fromJson(json).listapostazioni.first.name);
    });
  });
}*/
