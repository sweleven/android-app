import 'package:block_covid/api/ApiServiceCleaner.dart';
import 'package:block_covid/model/Stanza.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'ApiServiceCleaner_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  final String _baseUrlHttp = '192.168.210.40';
  final String _baseInventory = '/api/inventory';
  final String _baseCleanings = '/api/cleanings';
  final ApiServiceCleaner _apiServiceCleaner = ApiServiceCleaner();
  group('http get', () {
    test('getStanzeSporche', () async {
      final client = MockClient();
      String jsonResponse =
          '[{"clean":false,"enabled":true,"_id":"60a50aece1bbc100280948b6","name":"Workspace12","rfid":"workspace0102","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":false,"enabled":true,"_id":"60a50b00e1bbc100280948b7","name":"Workspace13","rfid":"workspace0103","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":false,"enabled":true,"_id":"60a50b1ee1bbc100280948b8","name":"Workspace21","rfid":"workspace0201","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true}]';
      when(client.get(
              Uri.http(
                _baseUrlHttp,
                _baseInventory + '/workspaces/dirty',
              ),
              headers: {'Authorization': 'Bearer '}))
          .thenAnswer(
              (realInvocation) async => http.Response(jsonResponse, 200));
      String jsonResponse2 =
          '[{"enabled":true,"_id":"60a50a56e1bbc100280948b2","name":"Stanza1","rfid":"stanza0001","__v":0,"workspaces":[{"clean":true,"enabled":true,"_id":"60a50ac9e1bbc100280948b5","name":"Workspace11","rfid":"workspace0101","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":false,"enabled":true,"_id":"60a50aece1bbc100280948b6","name":"Workspace12","rfid":"workspace0102","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":false,"enabled":true,"_id":"60a50b00e1bbc100280948b7","name":"Workspace13","rfid":"workspace0103","room":"60a50a56e1bbc100280948b2","__v":0,"available":true}],"workspaces_total":3},{"enabled":true,"_id":"60a50a6ee1bbc100280948b3","name":"Stanza2","rfid":"stanza0002","__v":0,"workspaces":[{"clean":false,"enabled":true,"_id":"60a50b1ee1bbc100280948b8","name":"Workspace21","rfid":"workspace0201","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a50b31e1bbc100280948b9","name":"Workspace22","rfid":"workspace0202","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true}],"workspaces_total":2}]';

      when(client
          .get(Uri.http(_baseUrlHttp, _baseInventory + '/rooms', {'name': ''}),
              headers: {'Authorization': 'Bearer '})).thenAnswer(
          (realInvocation) async => http.Response(jsonResponse2, 200));

      expect(await _apiServiceCleaner.getStanzeSporche(client),
          isA<List<Stanza>>());
    });
    test('getStanzePuliteOggi', () async {
      final client = MockClient();
      String oggi =
          DateFormat('yyyy-MM-dd').format(DateTime.now()) + 'T20:36:30.277Z';
      String jsonResponse =
          '[{"_id":"60a6c84e160813002843231a","workspace":"60a50ac9e1bbc100280948b5","datetime":"' +
              oggi +
              '","user":"a","__v":0},{"_id":"60a6c84e160813002843231b","workspace":"60a50aece1bbc100280948b6","datetime":"' +
              oggi +
              '","user":"a","__v":0}]';
      when(client.get(
              Uri.http(
                _baseUrlHttp,
                _baseCleanings + '/cleanings',
              ),
              headers: {'Authorization': 'Bearer '}))
          .thenAnswer(
              (realInvocation) async => http.Response(jsonResponse, 200));
      String jsonResponse2 =
          '{"clean":true,"enabled":true,"_id":"60a50ac9e1bbc100280948b5","name":"Workspace11","rfid":"workspace0101","room":"60a50a56e1bbc100280948b2","__v":0,"available":true}';
      when(client.get(
              Uri.http(_baseUrlHttp,
                  _baseInventory + '/workspaces/60a50ac9e1bbc100280948b5'),
              headers: {'Authorization': 'Bearer '}))
          .thenAnswer(
              (realInvocation) async => http.Response(jsonResponse2, 200));
      String jsonResponse3 =
          '{"clean":false,"enabled":true,"_id":"60a50aece1bbc100280948b6","name":"Workspace12","rfid":"workspace0102","room":"60a50a56e1bbc100280948b2","__v":0,"available":true}';
      when(client.get(
              Uri.http(_baseUrlHttp,
                  _baseInventory + '/workspaces/60a50aece1bbc100280948b6'),
              headers: {'Authorization': 'Bearer '}))
          .thenAnswer(
              (realInvocation) async => http.Response(jsonResponse3, 200));
      String jsonResponse4 =
          '[{"enabled":true,"_id":"60a50a56e1bbc100280948b2","name":"Stanza1","rfid":"stanza0001","__v":0,"workspaces":[{"clean":true,"enabled":true,"_id":"60a50ac9e1bbc100280948b5","name":"Workspace11","rfid":"workspace0101","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":false,"enabled":true,"_id":"60a50aece1bbc100280948b6","name":"Workspace12","rfid":"workspace0102","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":false,"enabled":true,"_id":"60a50b00e1bbc100280948b7","name":"Workspace13","rfid":"workspace0103","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a7aaaaef16d70042549a1f","name":"Workspace14","rfid":"workspace0104","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":false,"enabled":true,"_id":"60a7aabaef16d70042549a20","name":"Workspace15","rfid":"workspace0105","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a7ab19ef16d70042549a21","name":"Workspace16","rfid":"workspace0106","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a7baefef16d70042549a4b","name":"Workspace17","rfid":"sdgfdfghdgf","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60b10bd8355f850028d660ec","name":"Workspace18","rfid":"sdfsdfsdfsfsfsd","room":"60a50a56e1bbc100280948b2","__v":0,"available":true}],"workspaces_total":8},{"enabled":true,"_id":"60a50a6ee1bbc100280948b3","name":"Stanza2","rfid":"stanza0002","__v":0,"workspaces":[{"clean":false,"enabled":true,"_id":"60a50b1ee1bbc100280948b8","name":"Workspace21","rfid":"workspace0201","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a50b31e1bbc100280948b9","name":"Workspace22","rfid":"workspace0202","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a525a4e1bbc100280948bc","name":"Workspace23","rfid":"workspace023","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a7ab8fef16d70042549a28","name":"Workspace24","rfid":"workspace0204","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a7ac33ef16d70042549a2a","name":"Workspace25","rfid":"workspace0205","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a7ac3cef16d70042549a2b","name":"Workspace26","rfid":"workspace0206","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true}],"workspaces_total":6},{"enabled":true,"_id":"60a50a89e1bbc100280948b4","name":"Stanza3","rfid":"stanza0003","__v":0,"workspaces":[{"clean":true,"enabled":true,"_id":"60a50b55e1bbc100280948ba","name":"Workspace31","rfid":"workspace0301","room":"60a50a89e1bbc100280948b4","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a50b67e1bbc100280948bb","name":"Workspace32","rfid":"workspace0302","room":"60a50a89e1bbc100280948b4","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a7ac58ef16d70042549a2c","name":"Workspace33","rfid":"workspace0303","room":"60a50a89e1bbc100280948b4","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a7ac61ef16d70042549a2d","name":"Workspace34","rfid":"workspace0304","room":"60a50a89e1bbc100280948b4","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a7ac69ef16d70042549a2e","name":"Workspace35","rfid":"workspace0305","room":"60a50a89e1bbc100280948b4","__v":0,"available":true}],"workspaces_total":5}]';
      when(client
          .get(Uri.http(_baseUrlHttp, _baseInventory + '/rooms', {'name': ''}),
              headers: {'Authorization': 'Bearer '})).thenAnswer(
          (realInvocation) async => http.Response(jsonResponse4, 200));

      expect(await _apiServiceCleaner.getStanzePuliteOggi(client),
          isA<Map<String, List>>());
    });
  });
}
