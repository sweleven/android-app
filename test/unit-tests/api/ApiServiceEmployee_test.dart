import 'dart:convert';
import 'package:block_covid/api/ApiServiceEmployee.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:block_covid/model/Prenotazione.dart';
import 'package:block_covid/model/StatoPostazioneScan.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'ApiServiceEmployee_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  String _baseUrlHttp = '192.168.210.40';
  String _baseInventory = '/api/inventory';
  String _baseBooking = '/api/booking';
  String _baseCleanings = '/api/cleanings';
  String _baseUrlBooking = 'http://' + _baseUrlHttp + _baseBooking;
  final ApiServiceEmployee _apiServiceEmployee = ApiServiceEmployee();

  group('http post', () {
    test('inizioUso', () async {
      final client = MockClient();
      String jsonResponse = '';
      when(client.post(
          Uri.http(
            _baseUrlHttp,
            _baseBooking + '/check-in',
          ),
          body: jsonEncode(<String, String>{'rfid': 'workspace0101'}),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer '
          })).thenAnswer(
          (realInvocation) async => http.Response(jsonResponse, 200));
      String jsonResponse2 = '';
      when(client.post(
          Uri.http(
            _baseUrlHttp,
            _baseCleanings + '/cleanings',
          ),
          body: jsonEncode({'rfid': 'workspace0101'}),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer '
          })).thenAnswer(
          (realInvocation) async => http.Response(jsonResponse2, 200));

      expect(
          await _apiServiceEmployee.inizioUso(
              client,
              Postazione(
                  id: '212',
                  name: '2fes',
                  room: 'esf',
                  rfid: 'workspace0101',
                  clean: true,
                  stato: StatoPostazioneScan.miaInizioSporca)),
          true);
    });
    test('fineUso', () async {
      final client = MockClient();
      String jsonResponse = '';
      when(client.post(
          Uri.http(
            _baseUrlHttp,
            _baseBooking + '/check-out',
          ),
          body: jsonEncode(<String, String>{'rfid': 'workspace0101'}),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer '
          })).thenAnswer(
          (realInvocation) async => http.Response(jsonResponse, 200));
      String jsonResponse2 = '';
      when(client.post(
          Uri.http(
            _baseUrlHttp,
            _baseCleanings + '/cleanings',
          ),
          body: jsonEncode({'rfid': 'workspace0101'}),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer '
          })).thenAnswer(
          (realInvocation) async => http.Response(jsonResponse2, 200));

      expect(
          await _apiServiceEmployee.fineUso(
              client,
              Postazione(
                  id: '212',
                  name: '2fes',
                  room: 'esf',
                  rfid: 'workspace0101',
                  clean: true,
                  stato: StatoPostazioneScan.miaInizioSporca),
              true),
          true);
    });
    test('createReservation', () async {
      final client = MockClient();
      Prenotazione p = Prenotazione(
          postazione: Postazione(
              id: '212',
              name: '2fes',
              room: 'esf',
              rfid: 'workspace0101',
              clean: true,
              stato: StatoPostazioneScan.miaInizioSporca),
          inizio: DateTime(2020, 02, 01),
          fine: DateTime(2020, 02, 02));
      String jsonResponse = 'OKAY';
      when(client.post(Uri.parse(_baseUrlBooking + '/bookings'),
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer '
              },
              body: jsonEncode(<String, String>{
                'workspace': p.postazione.id,
                'start_datetime': (p.inizio.subtract(Duration(hours: 2)))
                        .toIso8601String()
                        .substring(0, 23) +
                    'Z',
                'end_datetime': (p.fine.subtract(Duration(hours: 2)))
                        .toIso8601String()
                        .substring(0, 23) +
                    'Z'
              })))
          .thenAnswer(
              (realInvocation) async => http.Response(jsonResponse, 200));
      expect(await _apiServiceEmployee.createReservation(client, p), true);
    });
  });
  group('http get', () {
    test('getPrenotazioni', () async {
      final client = MockClient();
      String jsonResponse =
          '[{"cancellation_datetime":null,"checkout_datetime":null,"consumed":true,"_id":"60a511eeca155d002adc7cc5","workspace":"60a50ac9e1bbc100280948b5","start_datetime":1621430700000,"end_datetime":1621447200000,"user":"detomasi.andrea@studenti.unipd.it","submission_datetime":1621430766214,"__v":0}]';
      DateTime inizio = DateTime(2020);
      DateTime fine = DateTime(2022);
      bool cancellate = true;
      when(client.get(
              Uri.http(_baseUrlHttp, _baseBooking + '/bookings', {
                'start_datetime': inizio
                        .subtract(Duration(hours: 2))
                        .toIso8601String()
                        .substring(0, 23) +
                    'Z',
                'end_datetime': fine
                        .subtract(Duration(hours: 2))
                        .toIso8601String()
                        .substring(0, 23) +
                    'Z',
                'show_cancelled': cancellate.toString()
              }),
              headers: {'Authorization': 'Bearer '}))
          .thenAnswer(
              (realInvocation) async => http.Response(jsonResponse, 200));
      String jsonResponse2 =
          '{"clean":true,"enabled":true,"_id":"60a50ac9e1bbc100280948b5","name":"Workspace11","rfid":"workspace0101","room":"60a50a56e1bbc100280948b2","__v":0,"available":true}';
      when(client.get(
              Uri.http(_baseUrlHttp,
                  _baseInventory + '/workspaces/60a50ac9e1bbc100280948b5'),
              headers: {'Authorization': 'Bearer '}))
          .thenAnswer(
              (realInvocation) async => http.Response(jsonResponse2, 200));

      expect(
          await _apiServiceEmployee.getPrenotazioni(
              client, inizio, fine, cancellate),
          isA<List<Prenotazione>>());
    });
    test('postazioniDisponibili', () async {
      final client = MockClient();
      String jsonResponse =
          '[{"clean":true,"enabled":true,"_id":"60a52760e39d9a003563deb6","name":"Postazione-1","rfid":"1111111111","room":"60a52701e39d9a003563deb3","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a52778e39d9a003563deb7","name":"Postazione-2","rfid":"22222222222","room":"60a52701e39d9a003563deb3","__v":0,"available":true}]';
      DateTime inizio = DateTime(2020);
      DateTime fine = DateTime(2022);
      when(client.get(
          Uri.http(_baseUrlHttp, _baseBooking + '/availability/workspaces', {
            'start_datetime': inizio.toIso8601String().substring(0, 23) + 'Z',
            'end_datetime': fine.toIso8601String().substring(0, 23) + 'Z'
          }),
          headers: {
            'Authorization': 'Bearer '
          })).thenAnswer(
          (realInvocation) async => http.Response(jsonResponse, 200));
      expect(
          await _apiServiceEmployee.postazioniDisponibili(
              client, '60a52701e39d9a003563deb3', inizio, fine),
          isA<List<Postazione>>());
    });
    test('getPostazioneFromRfid', () async {
      final client = MockClient();
      String jsonResponse =
          '{"clean":false,"enabled":true,"_id":"60a50aece1bbc100280948b6","name":"Workspace12","rfid":"workspace0102","room":"60a50a56e1bbc100280948b2","__v":0,"available":true}';
      when(client.get(
              Uri.http(
                _baseUrlHttp,
                _baseInventory + '/workspaces/rfid/workspace0102',
              ),
              headers: {'Authorization': 'Bearer '}))
          .thenAnswer(
              (realInvocation) async => http.Response(jsonResponse, 200));
      expect(
          await _apiServiceEmployee.getPostazioneFromRfid(
              client, 'workspace0102'),
          isA<Postazione>());
    });
  });
  group('http delete', () {
    test('eliminaPrenotazione', () async {
      final client = MockClient();
      String jsonResponse = '';
      Prenotazione p = Prenotazione(
          postazione: Postazione(
              id: '212',
              name: '2fes',
              room: 'esf',
              rfid: 'workspace0101',
              clean: true,
              stato: StatoPostazioneScan.miaInizioSporca),
          inizio: DateTime(2020, 02, 01),
          fine: DateTime(2020, 02, 02));
      when(client.delete(Uri.parse(_baseUrlBooking + '/bookings/' + p.id),
              headers: {'Authorization': 'Bearer '}))
          .thenAnswer(
              (realInvocation) async => http.Response(jsonResponse, 200));
      expect(await _apiServiceEmployee.eliminaPrenotazione(client, p), true);
    });
  });
}
