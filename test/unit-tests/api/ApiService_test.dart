// https://flutter.dev/docs/cookbook/testing/unit/mocking
// flutter pub run build_runner build

import 'package:block_covid/api/ApiService.dart';
import 'package:block_covid/model/Postazione.dart';
import 'package:block_covid/model/Stanza.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'dart:convert';
import 'ApiService_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  String _baseUrlHttp = '192.168.210.40';
  String _baseInventory = '/api/inventory';
  String _baseCleanings = '/api/cleanings';
  final ApiService _apiService = ApiService();
  group('http get', () {
    test('getRooms', () async {
      final client = MockClient();
      String jsonResponse =
          '[{"enabled":true,"_id":"60a50a56e1bbc100280948b2","name":"Stanza1","rfid":"stanza0001","__v":0,"workspaces":[{"clean":true,"enabled":true,"_id":"60a50ac9e1bbc100280948b5","name":"Workspace11","rfid":"workspace0101","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":false,"enabled":true,"_id":"60a50aece1bbc100280948b6","name":"Workspace12","rfid":"workspace0102","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":false,"enabled":true,"_id":"60a50b00e1bbc100280948b7","name":"Workspace13","rfid":"workspace0103","room":"60a50a56e1bbc100280948b2","__v":0,"available":true}],"workspaces_total":3},{"enabled":true,"_id":"60a50a6ee1bbc100280948b3","name":"Stanza2","rfid":"stanza0002","__v":0,"workspaces":[{"clean":false,"enabled":true,"_id":"60a50b1ee1bbc100280948b8","name":"Workspace21","rfid":"workspace0201","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true},{"clean":true,"enabled":true,"_id":"60a50b31e1bbc100280948b9","name":"Workspace22","rfid":"workspace0202","room":"60a50a6ee1bbc100280948b3","__v":0,"available":true}],"workspaces_total":2}]';
      when(client
          .get(Uri.http(_baseUrlHttp, _baseInventory + '/rooms', {'name': ''}),
              headers: {'Authorization': 'Bearer '})).thenAnswer(
          (realInvocation) async => http.Response(jsonResponse, 200));

      expect(await _apiService.getRooms(client), isA<List<Stanza>>());
    });
    test('getStanzaFromRfid', () async {
      final client = MockClient();
      String jsonResponse =
          '{"enabled":true,"_id":"60a50a56e1bbc100280948b2","name":"Stanza1","rfid":"stanza0001","__v":0,"workspaces":[{"clean":true,"enabled":true,"_id":"60a50ac9e1bbc100280948b5","name":"Workspace11","rfid":"workspace0101","room":"60a50a56e1bbc100280948b2","__v":0,"available":true},{"clean":false,"enabled":true,"_id":"60a50aece1bbc100280948b6","name":"Workspace12","rfid":"workspace0102","room":"60a50a56e1bbc100280948b2","__v":0,"available":true}],"workspaces_total":2}';
      String tag = 'stanza0001';
      when(client.get(
              Uri.http(
                _baseUrlHttp,
                _baseInventory + '/rooms/rfid/$tag',
              ),
              headers: {'Authorization': 'Bearer '}))
          .thenAnswer(
              (realInvocation) async => http.Response(jsonResponse, 200));

      expect(await _apiService.getStanzaFromRfid(client, tag), isA<Stanza>());
    });
    test('getPostazioneFromId', () async {
      final client = MockClient();
      String jsonResponse =
          '{"clean":true,"enabled":true,"_id":"60a50ac9e1bbc100280948b5","name":"Workspace11","rfid":"workspace0101","room":"60a50a56e1bbc100280948b2","__v":0,"available":true}';
      String id = '60a50ac9e1bbc100280948b5';
      when(client
          .get(Uri.http(_baseUrlHttp, _baseInventory + '/workspaces/$id'),
              headers: {'Authorization': 'Bearer '})).thenAnswer(
          (realInvocation) async => http.Response(jsonResponse, 200));

      expect(
          await _apiService.getPostazioneFromId(client, id), isA<Postazione>());
    });
  });

  group('http POST', () {
    test('pulizia', () async {
      final client = MockClient();
      String jsonRespone = '';
      when(client.post(
          Uri.http(
            _baseUrlHttp,
            _baseCleanings + '/cleanings',
          ),
          body: jsonEncode({'rfid': 'workspace0101'}),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer '
          })).thenAnswer(
          (realInvocation) async => http.Response(jsonRespone, 200));
      expect(await _apiService.pulisci(client, 'workspace0101'), true);
    });
  });
}
