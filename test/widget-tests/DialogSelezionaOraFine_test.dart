// ignore_for_file: unused_local_variable
import 'package:block_covid/extras/dialogs/DialogSelezionaOraFine.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  // Define a test. The TestWidgets function also provides a WidgetTester
  // to work with. The WidgetTester allows you to build and interact
  // with widgets in the test environment.
  testWidgets(
      'MyWidget has TextWidgets inside that Depend on the time you are passing is valid',
      (WidgetTester tester) async {
    // Create the widget by telling the tester to build it.
    await tester.pumpWidget(DialogSelezionaOraFine(DateTime.now()));
  });
}
