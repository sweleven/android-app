// ignore_for_file: unused_local_variable
// Imports the Flutter Driver API.
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('HomeCleaner App', () {
    // First, define the Finders and use them to locate widgets from the
    // test suite. Note: the Strings provided to the `byValueKey` method must
    // be the same as the Strings we used for the Keys in step 1.

    //HomeCleaner screen
    final exitButton = find.byValueKey('ExitButton');

    //LogOut Dialog
    final logoutDialog = find.byValueKey('LogoutDialog');
    final checkButton = find.byValueKey('checkButton');
    final closeButton = find.byValueKey('closeButton');

    //Tasto Stanza usate
    final stanzeUsateButton = find.byValueKey('StanzeUsateButton');

    //Tasto Stanze igienizzate
    final stanzeIgienizzateButton = find.byValueKey('StanzeIgienizzateButton');

    //Nfc Dialog
    final nfcButton = find.byValueKey('NfcButton');
    final nfcDialog = find.byValueKey('NfcDialog');
    final exitButtonNfcDialog = find.byValueKey('ExitButtonNfcDialog');

    FlutterDriver driver;

    Future<bool> isPresent(SerializableFinder byValueKey,
        {Duration timeout = const Duration(seconds: 1)}) async {
      try {
        await driver.waitFor(byValueKey, timeout: timeout);
        return true;
      } catch (exception) {
        return false;
      }
    }

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    /*
    test('Logout andato a buon fine', () async {
      if (await isPresent(exitButton)) {
        await driver.tap(exitButton);
      }

      await driver.waitFor(logoutDialog);

      if (await isPresent(checkButton)) {
        await driver.tap(checkButton);
      }
    });

    test('Logout NON andato a buon fine', () async {
      if (await isPresent(exitButton)) {
        await driver.tap(exitButton);
      }

      await driver.waitFor(logoutDialog);

      if (await isPresent(closeButton)) {
        await driver.tap(closeButton);
      }
    });
*/
    test('Nfc Dialog + ExitButton', () async {
      //if (await isPresent(nfcButton)) {}

      await driver.tap(nfcButton);

      await driver.waitFor(nfcDialog);

      if (await isPresent(exitButtonNfcDialog)) {
        await driver.tap(exitButtonNfcDialog);
      }
    });
  });
}
